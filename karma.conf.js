// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html
'use strict';

module.exports = function (config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '',

        // testing framework to use (jasmine/mocha/qunit/...)
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            'src/main/webapp/app/vendor/components/angular/angular.js',
            'src/main/webapp/app/vendor/components/angular-mocks/angular-mocks.js',
            'src/main/webapp/app/vendor/components/angular-resource/angular-resource.js',
            'src/main/webapp/app/vendor/components/angular-cookies/angular-cookies.js',
            'src/main/webapp/app/vendor/components/angular-sanitize/angular-sanitize.js',
            'src/main/webapp/app/vendor/components/angular-ui-router/release/angular-ui-router.js',
            'src/main/webapp/app/vendor/components/angular-scroll/angular-scroll.js',
            'src/main/webapp/app/vendor/components/jquery/dist/jquery.js',
            'src/main/webapp/app/scripts/*.js',
            'src/main/webapp/app/scripts/**/*.js',
            'src/main/webapp/app/tests/mock/**/*.js',
            'src/main/webapp/app/tests/spec/**/*.js'
        ],

        // list of files / patterns to exclude
        exclude: [],

        // web server port
        port: 8081,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        // here we specify which of the files we want to appear in the coverage report
        preprocessors: {
            'src/main/webapp/app/scripts/**/*.js': ['coverage']
        },

        // add the coverage plugin
        plugins: [ 'karma-jasmine', 'karma-firefox-launcher', 'karma-chrome-launcher', 'karma-coverage', 'karma-phantomjs-launcher'],

        // add coverage to reporters
        reporters: ['dots', 'coverage'],

        // tell karma how you want the coverage results
        coverageReporter: {
            type : 'html',
            // where to store the report
            dir : 'coverage/'
        }
    });
};
