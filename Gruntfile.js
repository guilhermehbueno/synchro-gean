'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    grunt.initConfig({

        config: {
            path: 'src/main/webapp/app',
            vendor: 'src/main/webapp/app/vendor/components',
            dist: 'src/main/webapp/dist',
            report: 'reports'
        },

        clean: {
            dev: {
                src: ['<%= config.dist %>']
            }
        },

        less: {
            development: {
                options: {
//                    paths: ["assets/css"]
                },
                files: {
                    '<%= config.dist %>/stylesheets/main.css': ['<%= config.path %>/stylesheets/main.less']
                }
            }
//            ,
//            production: {
//                options: {
//                    paths: ["assets/css"],
//                    cleancss: true,
//                    modifyVars: {
//                        imgPath: '"http://mycdn.com/path/to/images"',
//                        bgColor: 'red'
//                    }
//                },
//                files: {
//                    "path/to/result.css": "path/to/source.less"
//                }
//            }
        },

        watch: {
            scripts: {
                files: ['<%= config.path %>/scripts/**/*.js'],
                tasks: ['default']
            },
            less: {
                files: ['<%= config.path %>/stylesheets/**/*.less'],
                tasks: ['default']
            },
            jsTest: {
                files: ['<%= config.path %>/scripts/test/spec/**/' +
                    '*.js'],
                tasks: ['default']
            },
            gruntfile: {
                files: ['Gruntfile.js'],
                tasks: ['default']
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= config.path %>/scripts/{,*/}*.js'
            ],
            test: {
                options: {
                    jshintrc: '<%= config.path %>/tests/.jshintrc'
                },
                src: ['<%= config.path %>/tests/spec/{,*/}*.js']
            }
        },

        copy: {
//            css: {
//                files: [
//                    {
//                        expand: true,
//                        flatten: true,
//                        src: ['<%= config.dist %>/css/main.css'],
//                        dest: '<%= config.dist %>/css'
//                    }
//                ]
//            },
            fonts: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.path %>/vendor/components/bootstrap/fonts/*'],
                        dest: '<%= config.dist %>/fonts'
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.path %>/images/**/*'],
                        dest: '<%= config.dist %>/images'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.path %>/vendor/components/todc-bootstrap-full/dist/images/*'],
                        dest: '<%= config.dist %>/images'
                    }
                ]
            }
        },

        concat: {
            components: {
                src: ['<%= config.vendor %>/jquery/dist/jquery.js',
                    '<%= config.vendor %>/underscore/underscore.js',
                    '<%= config.vendor %>/jquery-waypoints/waypoints.js',
                    '<%= config.vendor %>/angular/angular.js',
                    '<%= config.vendor %>/angular-resource/angular-resource.js',
                    '<%= config.vendor %>/angular-cookies/angular-cookies.js',
                    '<%= config.vendor %>/angular-sanitize/angular-sanitize.js',
                    '<%= config.vendor %>/angular-ui-router/release/angular-ui-router.js',
                    '<%= config.vendor %>/angular-scroll/angular-scroll.js',
                    '<%= config.vendor %>/bootstrap/dist/js/bootstrap.js'
                ],
                dest: '<%= config.dist %>/scripts/components.js'
            },
            dev: {
                src: ['<%= config.dist %>/scripts/components.js',
                    '<%= config.path %>/scripts/app.js',
                    '<%= config.path %>/scripts/services/*.js',
                    '<%= config.path %>/scripts/controllers/*.js',
                    '<%= config.path %>/scripts/directives/*.js'

                ],
                dest: '<%= config.dist %>/scripts/development.js'
            }
        },

        uglify: {
            prod: {
                files: {
                    '<%= config.dist %>/prod.min.js': ['<%= config.dist %>/scripts/development.js']
                }
            }
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        },

        plato: {
            default: {
                options : {
                    jshint : grunt.file.readJSON('.jshintrc')
                },
                files: {
                    '<%= config.report %>/app': ['<%= config.path %>/scripts/**/*.js']
                }
            },
            test: {
                options : {
                    jshint : grunt.file.readJSON('src/main/webapp/app/tests/.jshintrc')
                },
                files: {
                    '<%= config.report %>/tests': [ '<%= config.path %>/tests/**/*.js']
                }
            }
        }

    });

    grunt.registerTask('default', [
        'jshint',
        'clean:dev',
        'less:development',
        'copy',
        'concat:components',
        'concat:dev',
        'plato:default'
    ]);


    grunt.registerTask('test', [
        'jshint',
        'clean:dev',
        'less:development',
        'copy',
        'concat:components',
        'concat:dev',
        'plato:default',
        'plato:test',
        'karma'
    ]);

    grunt.registerTask('prod', [
        'default',
        'uglify:prod'
    ]);
};