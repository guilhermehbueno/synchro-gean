package br.com.synchro.gean.business.service;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.repository.ClassificacaoEanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Cenarios de teste para o servico de votacao.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see VotoService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class ClassificacaoEanServiceTest {

    @Autowired
    private ClassificacaoEanService classificacaoEanService;

    @Autowired
    private ClassificacaoEanRepository classificacaoEanRepository;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Test
    public void test1() throws Exception {
        classificacaoEanService.load(ClassificacaoEanServiceTest.class.getResourceAsStream("/table.csv"));
        assertEquals(6, classificacaoEanRepository.count());
    }

}
