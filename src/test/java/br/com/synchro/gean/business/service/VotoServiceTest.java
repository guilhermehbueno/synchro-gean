package br.com.synchro.gean.business.service;

import br.com.synchro.gean.business.exception.ReferenceNotFoundException;
import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import br.com.synchro.gean.gateway.persistence.repository.VotoRepository;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertEquals;

/**
 * Cenarios de teste para o servico de votacao.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.business.service.VotoService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class VotoServiceTest {

    @Autowired
    private VotoService votoService;

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private DataGenerator dataGenerator;

    private GeanUser bobEsponja;
    private Funcionalidade funcionalidade;


    @Before
    public void setup() {
        bobEsponja = dataGenerator.createBobEsponja();
        funcionalidade = dataGenerator.createFuncionalidade("Quero monitoramento!", bobEsponja, 0);
    }

    /**
     * Dado que sou um usuario regularmente registrado no sistema.
     * E desejo registrar meu voto para uma funcionalidade.
     * Quando aciono o metodo FuncionalidadeService.votar()
     * Entao verifico que meu voto foi registrado.
     */
    @Test
    public void test1() throws Exception {
        votoService.votar(new Voto(bobEsponja, Voto.TipoInformacao.FUNCIONALIDADE, funcionalidade.getId(), true));
        assertEquals(1, votoRepository.findByIdInformacaoAndTipoInformacao(funcionalidade.getId(), Voto.TipoInformacao.FUNCIONALIDADE).size());
    }

    /**
     * Dado que NAO sou um usuario do sistema.
     * E desejo registrar meu voto para uma funcionalidade.
     * Quando aciono o metodo FuncionalidadeService.votar()
     * Entao verifico uma exception.
     */
    @Test(expected = ConstraintViolationException.class)
    public void test2() throws Exception {
        votoService.votar(new Voto(null, Voto.TipoInformacao.FUNCIONALIDADE, funcionalidade.getId(), true));
    }

    /**
     * Dado que sou um usuario regularmente registrado no sistema.
     * Quando aciono o metodo FuncionalidadeService.votar() passando uma instancia de voto com dado obrigatorio nao preenchido.
     * Entao verifico uma exception.
     */
    @Test(expected = ReferenceNotFoundException.class)
    public void test3() throws Exception {
        votoService.votar(new Voto(bobEsponja, null, funcionalidade.getId(), true));
    }


    /**
     * Dado que sou um usuario do sistema.
     * E desejo registrar meu voto para uma funcionalidade.
     * Quando aciono o metodo FuncionalidadeService.votar() passando uma instancia de voto com dado obrigatorio nao preenchido.
     * Entao verifico uma exception.
     */
    @Test(expected = ReferenceNotFoundException.class)
    public void test4() throws Exception {
        votoService.votar(new Voto(bobEsponja, Voto.TipoInformacao.FUNCIONALIDADE, null, true));
    }

    /**
     * Dado que sou um usuario do sistema.
     * E desejo registrar meu voto para uma funcionalidade.
     * Quando aciono o metodo FuncionalidadeService.votar() passando uma instancia de voto com dado obrigatorio nao preenchido.
     * Entao verifico uma exception.
     */
    @Test(expected = ConstraintViolationException.class)
    public void test5() throws Exception {
        votoService.votar(new Voto(bobEsponja, Voto.TipoInformacao.FUNCIONALIDADE, funcionalidade.getId(), null));
    }
}
