package br.com.synchro.gean.business.service;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Cenarios de teste para o servico de funcionalidade
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.business.service.FuncionalidadeService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class FuncionalidadeServiceTest {

    /**
     * Classe sob testes.
     */
    @Autowired
    private FuncionalidadeService funcionalidadeService;

    @Autowired
    private DataGenerator dataGenerator;

    private GeanUser bobEsponja;

    @Before
    public void setup() {
        bobEsponja = dataGenerator.createBobEsponja();
    }


    /**
     * Dado que desejo obter uma lista com as TOP 6 funcionalidades pedidas pelos usuarios.
     * Quando aciono o metodo Funcionalidade.listFuncionalidadesEmDestaque()
     * Entao recebo uma lista com as 6 funcionalidades em destaque.
     */
    @Test
    public void test1() throws Exception {
        //Cria uns votos para outros tipos de informacao, para assegurar que a
        // query esta considerando apenas funcionalidade e com votos positivos (curtidas).
        for (Long i = 100L; i < 110L; i++) {
            Faq faq = dataGenerator.createFaq("12345678", "P" + i, "R" + i);
            dataGenerator.createVoto(bobEsponja, Voto.TipoInformacao.FAQ, faq.getId(), true);
        }
        for (int i = 200; i < 205; i++) {
            dataGenerator.createFuncionalidade("Funcionalidade " + i, bobEsponja, i / 2, false);
        }

        dataGenerator.createFuncionalidade("Funcionalidade 1", bobEsponja, 40);
        dataGenerator.createFuncionalidade("Funcionalidade 2", bobEsponja, 400);
        dataGenerator.createFuncionalidade("Funcionalidade 3", bobEsponja, 300);
        dataGenerator.createFuncionalidade("Funcionalidade 4", bobEsponja, 10);

        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 5", bobEsponja, 5);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 6", bobEsponja, 5);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 7", bobEsponja, 5);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 8", bobEsponja, 5);

        List<Funcionalidade> top6 = funcionalidadeService.findFuncionalidadesEmDestaque();

        assertEquals(6, top6.size());
        assertEquals("Funcionalidade 2", top6.get(0).getDescription());
        assertEquals("Funcionalidade 3", top6.get(1).getDescription());
        assertEquals("Funcionalidade 1", top6.get(2).getDescription());
        assertEquals("Funcionalidade 8", top6.get(3).getDescription());
        assertEquals("Funcionalidade 7", top6.get(4).getDescription());
        assertEquals("Funcionalidade 6", top6.get(5).getDescription());
    }
}
