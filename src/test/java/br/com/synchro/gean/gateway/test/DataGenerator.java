package br.com.synchro.gean.gateway.test;

import br.com.synchro.gean.business.service.FuncionalidadeService;
import br.com.synchro.gean.business.service.VotoService;
import br.com.synchro.gean.gateway.persistence.mapping.*;
import br.com.synchro.gean.gateway.persistence.repository.ClassificacaoEanRepository;
import br.com.synchro.gean.gateway.persistence.repository.FaqRepository;
import br.com.synchro.gean.gateway.persistence.repository.FuncionalidadeRepository;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import br.com.synchro.gean.gateway.security.service.SecurityTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Component
public class DataGenerator {

    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;

    @Autowired
    private FuncionalidadeService funcionalidadeService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VotoService votoService;

    @Autowired
    private SecurityTokenService securityTokenService;

    @Autowired
    private ClassificacaoEanRepository classificacaoEanRepository;

    @Autowired
    private FaqRepository faqRepository;


    /**
     * Cria um funcionalidade com o nome dado e a quantidade de votos indicada.
     *
     * @param nome  Nome da funcionalidade.
     * @param votos Quantidade  de votos.
     * @return Funcionalidade criada.
     */
    public Funcionalidade createFuncionalidade(String nome, GeanUser user, Integer votos, Boolean curtiu) {
        Funcionalidade f = funcionalidadeRepository.save(new Funcionalidade(nome, user));

        for (int i = 0; i < votos; i++) {
            votoService.votar(new Voto(user, Voto.TipoInformacao.FUNCIONALIDADE, f.getId(), curtiu));
        }

        return f;
    }

    public Funcionalidade createFuncionalidade(String nome, GeanUser user, Integer votos) {
        return createFuncionalidade(nome, user, votos, true);
    }

    public Voto createVoto(GeanUser user, Voto.TipoInformacao tipoInformacao, Long idInformacao, Boolean curtiu) {
        return votoService.votar(new Voto(user, tipoInformacao, idInformacao, curtiu));
    }

    public GeanUser createUser(String username, String password, String email, String company) {
        return userRepository.save(new GeanUser(username, password, email, company));
    }

    public GeanUser createBobEsponja() {
        GeanUser bobEsponja = createUser("Bob Esponja", "12345", "bobesponja@fendadobiquini.com", "Siri Cascudo");
        bobEsponja.setRoles(Arrays.asList("USUARIO_FIRST_CLASS"));
        return bobEsponja;
    }

    public SecurityToken createSecurityToken(GeanUser bobEsponja) {
        return securityTokenService.createAndAdd(bobEsponja);
    }

    public Faq createFaq(String ean, String q, String a) {
        return faqRepository.save(new Faq(ean, q, a));
    }

    public ClassificacaoEan createClassificacaoEan(String ean, String ncm, String descricao) {
        return classificacaoEanRepository.save(new ClassificacaoEan(ean, ncm, descricao));
    }
}
