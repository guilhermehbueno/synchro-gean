package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.PersistenceConfig;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import java.util.Arrays;

import static org.junit.Assert.*;


/**
 * Casos de teste para o repositorio de Usuarios
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.GeanUser
 * @see br.com.synchro.gean.gateway.persistence.repository.UserRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, PersistenceConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class UserRepositoryTest {

    /**
     * Classe sob testes.
     */
    @Autowired
    private UserRepository userRepository;


    /**
     * Dado que desejo incluir um novo usuario no repositorio.
     * Quando aciono o metodo UserRepository.save() passando uma instancia.
     * Entao verifico que essa instancia foi gravada e um id lhe foi atribuido.
     * E verifico que ao acionar o metodo UserRepository.findByEmail() recebo o usuario que acabei de gravar.
     */
    @Test
    public void test1() {
        GeanUser user = new GeanUser("Paulo Freitas", "12345", "paulo.freitas@synchro.com.br", "Synnchro SolFIS");
        user.setRoles(Arrays.asList("ROLE_1"));

        assertNull(user.getId());

        user = userRepository.save(user);

        assertNotNull(user.getId());
        assertTrue(user.getId() > 0);

        GeanUser user2 = userRepository.findByEmail("paulo.freitas@synchro.com.br");
        assertNotNull(user2);
        assertEquals(user2.getId(), user.getId());
        assertNotNull(user2.getRoles());
        assertEquals("ROLE_1", user2.getRoles().get(0));

    }


    /**
     * @param userRepository
     * @see br.com.synchro.gean.gateway.persistence.repository.UserRepository
     */
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
