package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.*;
import br.com.synchro.gean.gateway.persistence.repository.PesquisaClassificacaoRepository;
import br.com.synchro.gean.gateway.persistence.repository.VotoRepository;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Casos de teste para o Controller de Pesquisa.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.controller.ClassificacaoController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
public class ClassificacaoControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private DataGenerator dataGenerator;

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private PesquisaClassificacaoRepository pesquisaClassificacaoRepository;

    private GeanUser bobEsponja;
    private ClassificacaoEan classificacaoEan;
    private SecurityToken securityToken;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();

        classificacaoEan = dataGenerator.createClassificacaoEan("123456789", "23011010", "Mercadoria");
        dataGenerator.createFaq("123456789", "q1", "a1");
        dataGenerator.createFaq("123456789", "q2", "a2");

        bobEsponja = dataGenerator.createBobEsponja();
        securityToken = dataGenerator.createSecurityToken(bobEsponja);

    }

    /**
     * Dado que desejo consultar a classificacao para um dado codigo Ean, existente na base de classificacao.
     * Quando faco um GET em "/classificacao/123456789"
     * Entao verifico os dados da classificacao, da FAQ e das top 6 funcionalidades mais pedidas.
     * E o status 200.
     *
     * @throws Exception
     */
    @Test
    public void testFindClassificacaoByEan1() throws Exception {
        dataGenerator.createFuncionalidade("Funcionalidade 1", bobEsponja, 40);
        dataGenerator.createFuncionalidade("Funcionalidade 2", bobEsponja, 400);
        dataGenerator.createFuncionalidade("Funcionalidade 3", bobEsponja, 300);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 4", bobEsponja, 5);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 5", bobEsponja, 5);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 6", bobEsponja, 5);

        this.mockMvc.perform(get("/classificacao/123456789"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$ncm", is("23011010")))
                .andExpect(jsonPath("$descricao", is("Mercadoria")))
                .andExpect(jsonPath("$faqs", hasSize(2)))
                .andExpect(jsonPath("$faqs[0].ean", is("123456789")))
                .andExpect(jsonPath("$faqs[0].question", is("q1")))
                .andExpect(jsonPath("$faqs[0].answer", is("a1")))
                .andExpect(jsonPath("$faqs[1].ean", is("123456789")))
                .andExpect(jsonPath("$faqs[1].question", is("q2")))
                .andExpect(jsonPath("$faqs[1].answer", is("a2")))
                .andExpect(jsonPath("$funcionalidades", hasSize(6)))
                .andExpect(jsonPath("$funcionalidades[0].description", is("Funcionalidade 2")))
                .andExpect(jsonPath("$funcionalidades[1].description", is("Funcionalidade 3")))
                .andExpect(jsonPath("$funcionalidades[2].description", is("Funcionalidade 1")))
                .andExpect(jsonPath("$funcionalidades[3].description", is("Funcionalidade 6")))
                .andExpect(jsonPath("$funcionalidades[4].description", is("Funcionalidade 5")))
                .andExpect(jsonPath("$funcionalidades[5].description", is("Funcionalidade 4")));

        //verifica se o registro da pesquisa ocorreu.
        List<PesquisaClassificacao> pesquisaClassificacao = pesquisaClassificacaoRepository.findByEan("123456789");
        assertEquals(1, pesquisaClassificacao.size());
        assertTrue(pesquisaClassificacao.get(0).getEncontrou());
    }

    /**
     * Dado que desejo consultar a classificacao para um dado codigo Ean, INEXISTENTE na base de classificacao.
     * Quando faco um GET em "/classificacao/0000000"
     * Entao verifico uma resposta vazia.
     * E o status 204 (No Content).
     *
     * @throws Exception
     */
    @Test
    public void testFindClassificacaoByEan2() throws Exception {
        this.mockMvc.perform(get("/classificacao/00000000"))
                .andExpect(status().isNoContent())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("{}"));

        //verifica se o registro da pesquisa ocorreu.
        List<PesquisaClassificacao> pesquisaClassificacao = pesquisaClassificacaoRepository.findByEan("00000000");
        assertEquals(1, pesquisaClassificacao.size());
        assertFalse(pesquisaClassificacao.get(0).getEncontrou());
    }

    /**
     * Dado que desejo dar um like para uma classificacao
     * Quando faco um post em /classificacao/like/{id}
     * Entao verifico uma resposta 201
     */
    @Test
    public void test3() throws Exception {
        MockHttpServletRequestBuilder post = post("/classificacao/like/" + classificacaoEan.getId())
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail());

        this.mockMvc.perform(post)
                .andExpect(status().isCreated());

        List<Voto> votos = votoRepository.findByIdInformacaoAndTipoInformacao(classificacaoEan.getId(), Voto.TipoInformacao.CLASSIFICACAO);
        assertEquals(1, votos.size());
        assertTrue(votos.get(0).getCurtiu());
    }

    /**
     * Dado que desejo dar um like para uma classificacao
     * Quando faco um post em /classificacao/like/{id} passando um id invalido
     * Entao verifico uma resposta 400
     */
    @Test
    public void test4() throws Exception {
        MockHttpServletRequestBuilder post = post("/classificacao/like/12")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail());

        this.mockMvc.perform(post)
                .andExpect(status().isBadRequest());

        assertEquals(0, votoRepository.findByIdInformacaoAndTipoInformacao(
                classificacaoEan.getId(), Voto.TipoInformacao.CLASSIFICACAO).size());
    }


    /**
     * Dado que desejo dar um UNLIKE para uma classificacao
     * Quando faco um post em /classificacao/like/{id}
     * Entao verifico uma resposta 201
     */
    @Test
    public void test5() throws Exception {
        MockHttpServletRequestBuilder post = post("/classificacao/like/" + classificacaoEan.getId())
                .param("not", "1")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail());

        this.mockMvc.perform(post)
                .andExpect(status().isCreated());

        List<Voto> votos = votoRepository.findByIdInformacaoAndTipoInformacao(classificacaoEan.getId(), Voto.TipoInformacao.CLASSIFICACAO);
        assertEquals(1, votos.size());
        assertFalse(votos.get(0).getCurtiu());
    }


}
