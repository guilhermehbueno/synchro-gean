package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Casos de teste para garantir o funcionamento  das regras de seguranca.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class UserControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();
    }

    /**
     * Dado que desejo me registrar como usuario.
     * Quanddo faco um POST em /user passando um JSON contendo meus dados cadastrais.
     * Entao verifico uma resposta 201.
     */
    @Test
    public void test1() throws Exception {
        assertNull("Esperava NAO encontrar um usuario com email bobesponja@fendadobiquini.com, pois este teste ira' cria-lo.",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));

        MockHttpServletRequestBuilder userPost = post("/user")
                .content("{\"username\": \"Bob Esponja\", \"password\": \"12345\", \"email\": \"bobesponja@fendadobiquini.com\", \"company\": \"Siri Cascudo\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(userPost)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$token", not(isEmptyOrNullString())))
                .andExpect(jsonPath("$expiresIn").exists())
                .andExpect(jsonPath("$user.password").doesNotExist())
                .andExpect(jsonPath("$user.username", is("Bob Esponja")))
                .andExpect(jsonPath("$user.email", is("bobesponja@fendadobiquini.com")))
                .andExpect(jsonPath("$user.company", is("Siri Cascudo")))
                .andExpect(jsonPath("$user.roles", hasSize(1)))
                .andExpect(jsonPath("$user.roles[0]", is("USUARIO_FIRST_CLASS")));

        GeanUser u = userRepository.findByEmail("bobesponja@fendadobiquini.com");
        assertNotNull("Esperava encontrar o usuario criado por email", u);
        assertTrue(passwordEncoder.matches("12345", u.getPassword()));
    }

    /**
     * Dado que desejo me registrar como usuario.
     * Quanddo faco um POST em /user passando um JSON INVALIDO.
     * Entao verifico uma resposta 400.
     */
    @Test
    public void test2() throws Exception {
        assertNull("Esperava NAO encontrar um usuario com email bobesponja@fendadobiquini.com, pois este teste ira' cria-lo.",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));

        MockHttpServletRequestBuilder userPost = post("/user")
                .content("{\"username\": \"Bob Esponja\", \"password\": \"12345\", \"non_email\": \"bobesponja@fendadobiquini.com\", \"company\": \"Siri Cascudo\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(userPost)
                .andExpect(status().isBadRequest())
                .andExpect(content().string(""));


        assertNull("Esperava NAO encontrar o usuario criado por email",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));
    }

    /**
     * Dado que desejo me registrar como usuario.
     * Quanddo faco um POST em /user passando um JSON com o campo email nao preenchido (obrigatorio).
     * Entao verifico uma resposta 400.
     */
    @Test
    public void test3() throws Exception {
        assertNull("Esperava NAO encontrar um usuario com email bobesponja@fendadobiquini.com, pois este teste ira' cria-lo.",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));

        MockHttpServletRequestBuilder userPost = post("/user")
                .content("{\"username\": \"\", \"password\": \"\", \"email\": \"\", \"company\": \"Siri Cascudo\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(userPost)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$errors", hasSize(3)))
                .andExpect(jsonPath("$errors", hasItem(hasEntry("field", "email"))))
                .andExpect(jsonPath("$errors", hasItem(hasEntry("field", "username"))))
                .andExpect(jsonPath("$errors", hasItem(hasEntry("field", "password"))));

        assertNull("Esperava NAO encontrar o usuario criado por email",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));
    }


    /**
     * Dado que desejo me registrar como usuario.
     * Quanddo faco um POST em /user passando um email em uso por outro usuario.
     * Entao verifico uma resposta 400, informando o ocorrido.
     */
    @Test
    public void test4() throws Exception {
        assertNull("Esperava NAO encontrar um usuario com email bobesponja@fendadobiquini.com, pois este teste ira' cria-lo.",
                userRepository.findByEmail("bobesponja@fendadobiquini.com"));

        GeanUser bobEsponja = new GeanUser("Bob Esponja", "12345", "bobesponja@fendadobiquini.com", "Siri Cascudo");
        bobEsponja.setRoles(Arrays.asList("USUARIO_FIRST_CLASS"));
        userRepository.save(bobEsponja);

        MockHttpServletRequestBuilder userPost = post("/user")
                .content("{\"username\": \"Bob Esponja 2\", \"password\": \"12345\", \"email\": \"bobesponja@fendadobiquini.com\", \"company\": \"Siri Cascudo\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(userPost)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$errors", hasSize(1)))
                .andExpect(jsonPath("$errors", hasItem(hasEntry("field", "email"))));
    }
}
