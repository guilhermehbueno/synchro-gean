package br.com.synchro.gean.gateway.security;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import br.com.synchro.gean.gateway.security.service.SecurityTokenService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Cenarios de teste para o servico de geracao e validacao de tokens de seguranca.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class SecurityTokenServiceTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    /**
     * Classe sob teste.
     */
    @Autowired
    private SecurityTokenService securityTokenService;

    @Autowired
    private UserRepository userRepository;

    private GeanUser bobEsponja;

    private GeanUser patrickEstrela;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();

        bobEsponja = new GeanUser("Bob Esponja", "12345", "bobesponja@fendadobiquini.com", "Siri Cascudo");
        bobEsponja.setRoles(Arrays.asList("USUARIO_FIRST_CLASS"));
        userRepository.save(bobEsponja);

        patrickEstrela = new GeanUser("Patrick Estrela", "12345", "patrick@fendadobiquini.com", "Siri Cascudo");
        userRepository.save(patrickEstrela);
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

    /**
     * Dado que desejo gerar um token de seguranca.
     * Quando aciono o metodo SecurityTokenService.createSecurityToken passando um User.
     * Entao verifico que um SecurityToken foi criado, havendo o token em si, o usuario, os papeis e a data de expiracao.
     */
    @Test
    public void test1() {
        SecurityToken securityToken = securityTokenService.createSecurityToken(bobEsponja);

        assertNotNull(securityToken);
        assertNotNull(securityToken.getToken());
        assertNotNull(securityToken.getUser());
        assertNotNull(securityToken.getRoles());
        assertNotNull(securityToken.getExpiresIn());
    }

    /**
     * Dado que desejo verificar se um token e' valido, ou seja, de fato pertencente a um dado usuario e esta dentro do prazo de validade.
     * Quando aciono o metodo SecurityTokenService.checkToken() passando o token e o email do usuario corretamente.
     * Entao recebo "true" como resultado.
     */
    @Test
    public void test2() {
        SecurityToken securityToken = securityTokenService.createAndAdd(bobEsponja);
        assertTrue(securityTokenService.checkToken(securityToken.getToken(), bobEsponja.getEmail()));
    }

    /**
     * Dado que desejo verificar se um token e' de fato pertencente a um dado usuario.
     * Quando aciono o metodo SecurityTokenService.checkToken() passando o token e o email do usuario INCORRETAMENTE.
     * Entao recebo "false" como resultado.
     */
    @Test
    public void test3() {
        SecurityToken securityToken = securityTokenService.createSecurityToken(bobEsponja);
        assertFalse(securityTokenService.checkToken(securityToken.getToken(), "BLAAAA"));
    }

    /**
     * Dado que adicionei um token ao repositorio.
     * Quando aciono o metodo SecurityTokenService.findByToken().
     * Entao devo receber o token referido.
     */
    @Test
    public void test4() {
        SecurityToken added = securityTokenService.createAndAdd(bobEsponja);
        SecurityToken loaded = securityTokenService.loadSecurityToken(added.getToken());
        assertEquals(added.getId(), loaded.getId());
    }

    /**
     * Dado que desejo validar um token expirado.
     * Quando aciono o metodo SecurityTokenService.checkToken() passando o token expirado e o email do usuario.
     * Entao recebo "false" como resultado.
     */
    @Test
    public void test5() throws Exception {
        //crio um token com validade de 100ms.
        SecurityToken securityToken = securityTokenService.createAndAdd(bobEsponja, 100L);
        Thread.sleep(200); // esperamos o token expirar.
        assertFalse(securityTokenService.checkToken(securityToken.getToken(), bobEsponja.getEmail()));
    }

    /**
     * Dado que desejo acessar um endpoint protegido.
     * Quando faco um request passando um token de autorizacao valido.
     * Entao devo receber uma resposta 201.
     *
     * @throws Exception
     */
    @Test
    public void test6() throws Exception {
        SecurityToken securityToken = securityTokenService.createAndAdd(bobEsponja);

        MockHttpServletRequestBuilder faqPost = post("/faq/create")
                .content("{\"ean\": 123456789, \"question\": \"q1\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isCreated());
    }

    /**
     * Dado que desejo acessar um endpoint protegido porem nao tenho o papel necessario.
     * Quando faco um request passando um token de autorizacao valido.
     * Entao devo receber uma resposta 403.
     *
     * @throws Exception
     */
    @Test
    public void test9() throws Exception {
        SecurityToken securityToken = securityTokenService.createAndAdd(patrickEstrela);

        MockHttpServletRequestBuilder faqPost = post("/faq")
                .content("{\"ean\": 123456789, \"question\": \"q1\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", patrickEstrela.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isForbidden());
    }

    /**
     * Dado que desejo acessar um endpoint protegido.
     * Quando faco um request sem passar um token de autorizacao.
     * Entao devo receber uma resposta 403.
     *
     * @throws Exception
     */
    @Test
    public void test7() throws Exception {
        SecurityToken securityToken = securityTokenService.createAndAdd(bobEsponja);

        MockHttpServletRequestBuilder faqPost = post("/faq/create")
                .content("{\"ean\": 123456789, \"question\": \"q1\"}")
                .header("Authorization", "")
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isUnauthorized());
    }

    /**
     * Dado que desejo acessar um endpoint protegido.
     * Quando faco um request passando  um token de autorizacao valido porem com email do subject incorreto..
     * Entao devo receber uma resposta 403.
     *
     * @throws Exception
     */
    @Test
    public void test8() throws Exception {
        SecurityToken securityToken = securityTokenService.createAndAdd(bobEsponja);

        MockHttpServletRequestBuilder faqPost = post("/faq/create")
                .content("{\"ean\": 123456789, \"question\": \"q1\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", "")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isUnauthorized());
    }

}
