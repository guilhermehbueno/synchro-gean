package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Cenarios de teste para o repositorios de votos.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class VotoRepositoryTest {

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private UserRepository userRepository;
    private GeanUser bobEsponja;

    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;
    private Funcionalidade funcionalidade;

    @Before
    public void setup() {
        bobEsponja = userRepository.save(new GeanUser("Bob Esponja", "12345", "bobesponja@fendadobiquini.com", "Siri Cascudo"));
        funcionalidade = funcionalidadeRepository.save(new Funcionalidade("Quero monitoramento!", bobEsponja));
    }

    /**
     * Dado que desejo registrar um voto para um certa funcionalidade.
     * Quando aciono o metodo VotoRepository.save() passando uma instancia.
     * Entao verifico que esta instancia foi gravada.
     */
    @Test
    public void test1() {
        Voto voto = new Voto(bobEsponja, Voto.TipoInformacao.FUNCIONALIDADE, funcionalidade.getId(), true);
        assertNull(voto.getId());
        voto = votoRepository.save(voto);
        assertTrue(voto.getId() > 0);
    }
}
