package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import br.com.synchro.gean.gateway.persistence.repository.FuncionalidadeRepository;
import br.com.synchro.gean.gateway.persistence.repository.VotoRepository;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Casos de teste para o Controller de Faq.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see FaqController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class FuncionalidadeControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private DataGenerator dataGenerator;

    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;

    @Autowired
    private VotoRepository votoRepository;

    @Autowired
    private PlatformTransactionManager transactionManager;

    private SecurityToken securityToken;

    private GeanUser bobEsponja;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();

        bobEsponja = dataGenerator.createBobEsponja();
        securityToken = dataGenerator.createSecurityToken(bobEsponja);
    }

    /**
     * Dado que desejo registrar um pedido de funcionalidade.
     * Quando realizo um POST passando um payload correto.
     * Entao verifico a resposta 201.
     */
    @Test
    public void test1() throws Exception {

        MockHttpServletRequestBuilder faqPost = post("/funcionalidade")
                .content("{\"descricao\": \"Seria bom ver IPI!\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isCreated());

        assertTrue(funcionalidadeRepository.count() == 1);
    }

    /**
     * Dado que desejo registrar um pedido de funcionalidade.
     * Quando realizo um POST passando um payload INCORRETO.
     * Entao verifico a resposta 400 - Bad Request.
     */
    @Test
    public void test2() throws Exception {

        MockHttpServletRequestBuilder faqPost = post("/funcionalidade")
                .content("{\"bla\": \"Seria bom ver IPI!\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isBadRequest());

        assertTrue(funcionalidadeRepository.count() == 0);
    }


    /**
     * Dado que desejo registrar um pedido de funcionalidade.
     * Quando realizo um POST passando um payload com o campo descricao vazio.
     * Entao verifico a resposta 400 - Bad Request.
     */
    @Test
    public void test3() throws Exception {

        MockHttpServletRequestBuilder faqPost = post("/funcionalidade")
                .content("{\"descricao\": \" \"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$errors", hasSize(1)));

        assertTrue(funcionalidadeRepository.count() == 0);
    }



    /**
     * Dado que desejo registrar meu voto para um pedido de funcionalidade.
     * Quando realizo um POST em /funcionalidade/votar/{id}.
     * Entao verifico a resposta 201 - Created.
     */
    @Test
    public void test5() throws Exception {
        Funcionalidade f = new Funcionalidade("Seria bom ver IPI!", bobEsponja);
        funcionalidadeRepository.save(f);

        MockHttpServletRequestBuilder faqPost = post("/funcionalidade/votar/" + f.getId())
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isCreated());

        assertEquals(1, votoRepository.findByIdInformacaoAndTipoInformacao(f.getId(), Voto.TipoInformacao.FUNCIONALIDADE).size());
    }

    /**
     * Dado que desejo registrar meu voto para um pedido de funcionalidade.
     * Quando realizo um POST em /funcionalidade/votar/{id} passando o ID de um funcionalidade inexistente.
     * Entao verifico a resposta 400 - Bad Request.
     */
    @Test
    public void test6() throws Exception {
        Funcionalidade f = new Funcionalidade("Seria bom ver IPI!", bobEsponja);
        funcionalidadeRepository.save(f);

        MockHttpServletRequestBuilder faqPost = post("/funcionalidade/votar/123")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$errors", hasSize(1)))
                .andExpect(jsonPath("$errors", hasItem(hasEntry("field", "idInformacao"))));

        assertTrue(transactionManager.getTransaction(null).isRollbackOnly());
    }




}
