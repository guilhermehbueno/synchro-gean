package br.com.synchro.gean.gateway.persistence.repository;


import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.PersistenceConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Casos de teste para o repositorio de Faq
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.Faq
 * @see br.com.synchro.gean.gateway.persistence.repository.FaqRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, PersistenceConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class FaqRepositoryTest {

    /**
     * Classe sob teste
     */
    @Autowired
    private FaqRepository faqRepository;

    /**
     * Dado que desejo incluir uma Faq na base de dados.
     * Quando aciono o metodo FaqRepositoryImpl.save() passando uma nova instancia.
     * Entao devo verificar que esta instancia foi gravada
     * E um ID foi gerado e atribuido a ela.
     */
    @Test
    public void testSave() {
        Faq faq = new Faq("123456789", "Pergunta", "Resposta");
        faq = faqRepository.save(faq);

        assertNotNull(faq.getId());
        assertTrue(faq.getId() > 0);
    }

    /**
     * Dado que existe uma Faq para o ean "123456789".
     * Quando aciono o metodo FaqRepositoryImpl.findByEan() passando este ean.
     * Entao devo receber a Faq correspondente.
     */
    @Test
    public void testFindByEan() {
        Faq faq = new Faq("123456789", "Pergunta", "Resposta");
        faq = faqRepository.save(faq);

        List<Faq> faqFounded = faqRepository.findByEan("123456789");
        assertNotNull(faq);
        assertTrue(faqFounded.size() == 1);
        assertEquals(faqFounded.get(0).getId(), faq.getId());
    }

    /**
     * Permite injecao do repositorio sob teste.
     *
     * @param faqRepository Instancia proveniente do Spring,
     */
    public void setFaqRepository(FaqRepository faqRepository) {
        this.faqRepository = faqRepository;
    }
}
