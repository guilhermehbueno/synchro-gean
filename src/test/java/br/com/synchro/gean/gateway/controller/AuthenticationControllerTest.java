package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.persistence.repository.SecurityTokenRepository;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Casos de teste para garantir o funcionamento  das regras de seguranca.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class AuthenticationControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityTokenRepository securityTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    private GeanUser bobEsponja;

    @Before
    public void setup() {
        bobEsponja = new GeanUser("Bob Esponja", passwordEncoder.encode("12345"), "bobesponja@fendadobiquini.com", "Siri Cascudo");
        bobEsponja.setRoles(Arrays.asList("USUARIO_FIRST_CLASS"));
        userRepository.save(bobEsponja);

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

    /**
     * Dado que sou o usuario "Bob Esponja", regularmente registrado no sistema.
     * Quando aciono o endpoint "/authentication" passando minhas credenciais de acesso.
     * Entao devo receber uma resposta contendo um token de acesso e meus dados e papeis.
     */
    @Test
    public void test1() throws Exception {
        MockHttpServletRequestBuilder authenticationPost = post("/authentication")
                .content("{\"email\": \"bobesponja@fendadobiquini.com\", \"password\": \"12345\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(authenticationPost)
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$token", not(isEmptyOrNullString())))
                .andExpect(jsonPath("$expiresIn").exists())
                .andExpect(jsonPath("$user.password").doesNotExist())
                .andExpect(jsonPath("$user.username", is(bobEsponja.getUsername())))
                .andExpect(jsonPath("$user.email", is(bobEsponja.getEmail())))
                .andExpect(jsonPath("$user.company", is(bobEsponja.getCompany())))
                .andExpect(jsonPath("$user.roles", hasSize(1)))
                .andExpect(jsonPath("$user.roles[0]", is("USUARIO_FIRST_CLASS")));
    }

    /**
     * Dado que NAO SOU um usuario registrado no sistema.
     * Quando aciono o endpoint "/authentication" passando minhas credenciais de acesso.
     * Entao devo receber uma resposta 403 - Forbidden.
     */
    @Test
    public void test2() throws Exception {
        MockHttpServletRequestBuilder authenticationPost = post("/authentication")
                .content("{\"username\": \"fuleco\", \"password\": \"12345\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(authenticationPost)
                .andExpect(status().isForbidden());
    }


    /**
     * Dado que sou o usuario "Bob Esponja", regularmente registrado no sistema.
     * Quando aciono o endpoint "/authentication" passando minhas credenciais de acesso INCORRETAMENTE.
     * Entao devo receber uma resposta 403 - Forbidden.
     */
    @Test
    public void test3() throws Exception {
        MockHttpServletRequestBuilder authenticationPost = post("/authentication")
                .content("{\"email\": \"bobesponja@fendadobiquini.com\", \"password\": \"BLAAaA\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(authenticationPost)
                .andExpect(status().isForbidden());
    }

    /**
     * Dado que sou o usuario "Bob Esponja", regularmente registrado no sistema.
     * Quando aciono o endpoint "/authentication" passando minhas credenciais de acesso.
     * Entao devo verificar que um token de acesso foi registrado em meu nome.
     */
    @Test
    public void test4() throws Exception {
        MockHttpServletRequestBuilder authenticationPost = post("/authentication")
                .content("{\"email\": \"bobesponja@fendadobiquini.com\", \"password\": \"12345\"}")
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(authenticationPost)
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        List<SecurityToken> userTokens = securityTokenRepository.findByUserEmail("bobesponja@fendadobiquini.com");
        assertNotNull(userTokens);
        assertEquals(1, userTokens.size());
    }

}
