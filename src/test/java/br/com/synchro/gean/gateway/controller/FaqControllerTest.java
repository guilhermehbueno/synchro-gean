package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import br.com.synchro.gean.gateway.persistence.repository.VotoRepository;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import java.util.List;

import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Casos de teste para o Controller de Faq.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.controller.FaqController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class FaqControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private DataGenerator dataGenerator;

    @Autowired
    private VotoRepository votoRepository;

    private SecurityToken securityToken;
    private GeanUser bobEsponja;
    private Faq faq;


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilters(springSecurityFilterChain).build();

        bobEsponja = dataGenerator.createBobEsponja();
        securityToken = dataGenerator.createSecurityToken(bobEsponja);
        faq = dataGenerator.createFaq("25584568", "Pergunta", "Resposta");
    }

    /**
     * Dado que desejo registrar uma nova pergunta (Faq) para um Ean.
     * Quando realizo um POST passando um payload correto.
     * Entao verifico a resposta contendo os dados da pergunta recem criada.
     *
     * @throws Exception
     */
    @Test
    public void testRegistrarPergunta() throws Exception {

        MockHttpServletRequestBuilder faqPost = post("/faq/create")
                .content("{\"ean\": 123456789, \"question\": \"q1\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$id", anything()))
                .andExpect(jsonPath("$ean", is("123456789")))
                .andExpect(jsonPath("$question", is("q1")));
    }

    /**
     * Dado que desejo registrar uma nova pergunta (Faq) para um Ean.
     * Quando realizo um POST passando um payload INCORRETO.
     * Entao verifico o status da resposta.
     *
     * @throws Exception
     */
    @Test
    public void testRegistrarPergunta1() throws Exception {

        MockHttpServletRequestBuilder faqPost = post("/faq/create")
                .content("{\"ean\": 123456789, \"bla\": \"q1\"}")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(faqPost)
                .andExpect(status().isBadRequest());
    }


    /**
     * Dado que desejo dar um like para uma classificacao
     * Quando faco um post em /classificacao/like/{id}
     * Entao verifico uma resposta 201
     */
    @Test
    public void test3() throws Exception {
        MockHttpServletRequestBuilder post = post("/faq/like/" + faq.getId())
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail());

        this.mockMvc.perform(post)
                .andExpect(status().isCreated());

        List<Voto> votos = votoRepository.findByIdInformacaoAndTipoInformacao(faq.getId(), Voto.TipoInformacao.FAQ);
        assertEquals(1, votos.size());
        assertTrue(votos.get(0).getCurtiu());
    }

    /**
     * Dado que desejo dar um like para uma classificacao
     * Quando faco um post em /classificacao/like/{id} passando um id invalido
     * Entao verifico uma resposta 400
     */
    @Test
    public void test4() throws Exception {
        MockHttpServletRequestBuilder post = post("/faq/like/12")
                .header("Authorization", securityToken.getToken())
                .header("Subject", bobEsponja.getEmail());

        this.mockMvc.perform(post)
                .andExpect(status().isBadRequest());

        assertEquals(0, votoRepository.findByIdInformacaoAndTipoInformacao(
                faq.getId(), Voto.TipoInformacao.FAQ).size());
    }




}
