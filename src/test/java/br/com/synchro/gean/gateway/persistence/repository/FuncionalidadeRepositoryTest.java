package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.TestConfig;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.test.DataGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Cenarios de teste para o repositorio de FeatureRequest.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, TestConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class FuncionalidadeRepositoryTest {

    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;

    private GeanUser bobEsponja;

    @Autowired
    private DataGenerator dataGenerator;

    @Before
    public void setup() {
        bobEsponja = dataGenerator.createBobEsponja();
    }

    /**
     * Dado que sou um usuario regularmente cadastrado no Gean.
     * Quando aciono o metodo FeatureRequestRepository.save() passando uma instancia preenchida.
     * Entao verifico que esta instancia foi gravada pelo mecanismo de persistencia.
     */
    @Test
    public void test1() {
        Funcionalidade fr = new Funcionalidade("Seria bom ver aliquota IPI", bobEsponja);
        assertNull(fr.getId());

        funcionalidadeRepository.save(fr);
        assertTrue(fr.getId() > 0);

        Funcionalidade fr1 = funcionalidadeRepository.findOne(fr.getId());
        assertNotNull(fr1.getOwner());
        assertNotNull(fr1.getDateCreated());
    }

    /**
     * Dado que possuo 4 funcionalidades com quantidade expressiva de votos.
     * Quando aciono o metodo FuncionalidadeRepository.findMaisVotadas() passando uma pagina com limite de 3 registros.
     * Entao recebo uma lista com 3 funcionalidades na ordem decrescente de contagem de votos.
     */
    @Test
    public void test2() {
        dataGenerator.createFuncionalidade("Funcionalidade 1", bobEsponja, 40);
        dataGenerator.createFuncionalidade("Funcionalidade 2", bobEsponja, 400);
        dataGenerator.createFuncionalidade("Funcionalidade 3", bobEsponja, 300);
        dataGenerator.createFuncionalidade("Funcionalidade 4", bobEsponja, 10);

        List<Funcionalidade> maisVotadas = funcionalidadeRepository.findMaisVotadas(new PageRequest(0, 3));

        assertEquals(3, maisVotadas.size());
        assertEquals("Funcionalidade 2", maisVotadas.get(0).getDescription());
        assertEquals("Funcionalidade 3", maisVotadas.get(1).getDescription());
        assertEquals("Funcionalidade 1", maisVotadas.get(2).getDescription());

        assertTrue(maisVotadas.get(0).getQuantidadeVotos() == 400L);
        assertTrue(maisVotadas.get(1).getQuantidadeVotos() == 300L);
        assertTrue(maisVotadas.get(2).getQuantidadeVotos() == 40L);
    }


    /**
     * Dado que possuo 4 funcionalidades criadas com diferenca de 100 ms.
     * Quando  aciono o metodo FuncionalidadeRepository.findMaisRecentes() passando uma pagina com limite de 3 registros.
     * Entao recebo uma lista com 3 funcionalidades na ordem decrescente de data de criacao.
     */
    @Test
    public void test3() throws Exception {
        dataGenerator.createFuncionalidade("Funcionalidade 1", bobEsponja, 40);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 2", bobEsponja, 400);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 3", bobEsponja, 300);
        Thread.sleep(100);
        dataGenerator.createFuncionalidade("Funcionalidade 4", bobEsponja, 10);

        List<Funcionalidade> maisRecentes = funcionalidadeRepository.findMaisRecentes(new PageRequest(0, 3));

        assertEquals(3, maisRecentes.size());
        assertEquals("Funcionalidade 4", maisRecentes.get(0).getDescription());
        assertEquals("Funcionalidade 3", maisRecentes.get(1).getDescription());
        assertEquals("Funcionalidade 2", maisRecentes.get(2).getDescription());
    }
}
