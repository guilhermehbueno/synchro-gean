package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Casos de teste para o repositorio de Usuarios
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.GeanUser
 * @see UserRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class SecurityTokenRepositoryTest {

    /**
     * Classe sob testes.
     */
    @Autowired
    private SecurityTokenRepository securityTokenRepository;

    @Autowired
    private UserRepository userRepository;

    /**
     * Dado que desejo incluir um novo usuario no repositorio.
     * Quando aciono o metodo UserRepository.save() passando uma instancia.
     * Entao verifico que essa instancia foi gravada e um id lhe foi atribuido.
     * E verifico que ao acionar o metodo UserRepository.findByEmail() recebo o usuario que acabei de gravar.
     */
    @Test
    public void test1() {
        GeanUser user = new GeanUser("Paulo Freitas", "12345", "paulo.freitas@synchro.com.br", "Synnchro SolFIS");
        user.setRoles(Arrays.asList("ROLE_1"));

        userRepository.save(user);

        SecurityToken securityToken = new SecurityToken(user, System.currentTimeMillis());
        securityToken.setToken("secrete_token");

        securityTokenRepository.save(securityToken);

        List<SecurityToken> userTokens = securityTokenRepository.findByUserEmail(user.getEmail());
        assertNotNull(userTokens);
        assertEquals(1, userTokens.size());


    }



}
