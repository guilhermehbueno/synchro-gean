package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.config.ApplicationConfig;
import br.com.synchro.gean.config.PersistenceConfig;
import br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

/**
 * Casos de teste para o repositorio de Classificacao de Eans
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan
 * @see br.com.synchro.gean.gateway.persistence.repository.ClassificacaoEanRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class ClassificacaoEanRepositoryTest {

    /**
     * Classe sob testes.
     */
    @Autowired
    private ClassificacaoEanRepository classificacaoEanRepository;


    /**
     * Dado que desejo incluir uma classificacao para um EAN
     * Quando aciono o metodo ClassificacaoEanRepository.save() passando uma instancia
     * Entao verifico que essa instancia foi gravada e um id  lhe foi atribuido.
     * E verifico que ao acionar o metodo ClassificacaoEanRepository.findByEan() recebo a ClassficiacaoEan /
     * que acabei de gravar.
     */
    @Test
    public void test1() {
        ClassificacaoEan ce = new ClassificacaoEan();
        ce.setEan("123456789");
        ce.setNcm("23011010");
        ce.setDescription("Mercadoria 1");

        assertNull(ce.getId());

        ce = classificacaoEanRepository.save(ce);

        assertNotNull(ce.getId());
        assertTrue(ce.getId() > 0);

        ClassificacaoEan cef = classificacaoEanRepository.findByEan("123456789");
        assertNotNull(cef);
        assertEquals(cef.getId(), ce.getId());
    }

    /**
     * @param classificacaoEanRepository
     * @see #classificacaoEanRepository
     */
    public void setClassificacaoEanRepository(ClassificacaoEanRepository classificacaoEanRepository) {
        this.classificacaoEanRepository = classificacaoEanRepository;
    }
}
