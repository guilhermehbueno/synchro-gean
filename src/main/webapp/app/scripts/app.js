'use strict';

var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
    return window._;
});

angular.module('geanApp', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ui.router',
        'duScroll',
        'underscore'
    ])
    .config(['$stateProvider', '$locationProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider) {

        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('home', {
                url: '/',
                views: {
                    'main': { templateUrl: 'app/views/main.html', controller: 'MainCtrl' },
                    'menu': { templateUrl: 'app/views/includes/public-menu.html' }
                }
            })
            .state('cadastro', {
                url: '/cadastro',
                views: {
                    'main': { templateUrl: 'app/views/register.html' },
                    'menu': { templateUrl: 'app/views/includes/result-menu.html' }
                }
            })
            .state('busca', {
                url: '/busca/:ean',
                views: {
                    'main': { templateUrl: 'app/views/result.html', controller: 'FinderCtrl'},
                    'menu': { templateUrl: 'app/views/includes/result-menu.html' }
                }
            })
            .state('login', {
                url: '/login',
                views: {
                    'main': { templateUrl: 'app/views/login.html'},
                    'menu': { templateUrl: 'app/views/includes/result-menu.html' }
                }
            });


        /* Register error provider that shows message on failed requests or redirects to login page on
         * unauthenticated requests */
        $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
            return {
                'responseError': function (rejection) {
                    var status = rejection.status;
                    var config = rejection.config;
                    var method = config.method;
                    var url = config.url;
                    if (status === 401) {
                        $rootScope.user = null;
                        $rootScope.loginError = 'Sua sessão expirou, faça o login novamente.';
                        $('#loginModal').modal('show');
                        $('#loginModal').on('hidden.bs.modal', function (e) {
                            $rootScope.loginError = null;
                        });
                    } else {
                        $rootScope.loginError = 'Usuário Inválido.';
                    }
                    return $q.reject(rejection);
                }
            };
        }]);


        /* Registers auth token interceptor, auth token is either passed by header or by query parameter
         * as soon as there is an authenticated user */
        $httpProvider.interceptors.push(['$q', '$rootScope', '$cookieStore', function ($q, $rootScope, $cookieStore) {
            return {
                'request': function (config) {
                    if (Boolean($rootScope.authToken) || Boolean($cookieStore.get('securityToken'))) {

                        if(!angular.isDefined($rootScope.authToken)){
                            var securityToken = $cookieStore.get('securityToken');

                            $rootScope.authToken = securityToken.token;
                            $rootScope.user = securityToken.user;
                        }

                        var authToken = $rootScope.authToken,
                            subject = $rootScope.user.email;
                        config.headers.Authorization = authToken;
                        config.headers.Subject = subject;
                    }
                    return config || $q.when(config);
                },
                'response': function(config){

                    var securityTokenJson = config.headers('new-security-token');

                    if(Boolean(securityTokenJson)){
                        var securityToken = angular.fromJson(securityTokenJson);
                        $cookieStore.put('securityToken', securityToken);

                        $rootScope.authToken = securityToken.token;
                        $rootScope.user = securityToken.user;
                    }

                    return config || $q.when(config);
                }
            };
        }]);

    }]);