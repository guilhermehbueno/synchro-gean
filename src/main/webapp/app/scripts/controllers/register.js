'use strict';

angular.module('geanApp')
    .controller('RegisterCtrl', ['$scope', '$state', '$rootScope', '$cookieStore', 'userService', function ($scope, $state, $rootScope, $cookieStore, userService) {

        $scope.user = {};
        $scope.passConfirm = null;

        $scope.createUser = function() {
            if ($scope.userForm.$valid) {
                userService.create($scope.user).then(function(data) {
                    $cookieStore.put('securityToken', data);

                    $rootScope.authToken = data.token;
                    $rootScope.user = data.user;
                    $state.go('home');
                });
            }
        };
    }]);
