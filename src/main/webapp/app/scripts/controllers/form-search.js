'use strict';

angular.module('geanApp')
    .controller('FormSearchCtrl', ['$scope', '$state', function ($scope, $state) {

        $scope.ean = null;
        $scope.search = function() {
            if ($scope.searchForm.$valid) {
                $state.go('busca', {ean: $scope.ean});
            } else {
                alert('Insira o código EAN');
            }
        };
    }]);
