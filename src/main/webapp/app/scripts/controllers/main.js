'use strict';

angular.module('geanApp')
    .controller('MainCtrl', ['$scope', '$document', function ($scope, $document) {

        $scope.goToSection = function (elementId, offSet) {
            var section = angular.element(document.getElementById(elementId));
            var scrollOffSet = offSet || 0;
            $document.scrollTop(section.context.offsetTop + scrollOffSet, 1000);
        };

        $scope.goToSearch = function () {
            $scope.goToSection('searchTitle', 300);
            $('#searchField').focus();
        };

        $scope.showLogin = function () {
            $('#loginModal').modal('show');
        };

        $scope.$on('$viewContentLoaded', function () {

            function parallax() {
                var scrollPosition = $(window).scrollTop();
                $('#parallax').css('top', (80 - (scrollPosition * 0.85)) + 'px');
                $('#hero').css('opacity', ((100 - scrollPosition / 2) * 0.01));
            }

            $(window).on('scroll', function (e) {
                parallax();
            });


            $('.animate').waypoint(function () {
                var animation = $(this).attr('data-animate');
                $(this).addClass(animation);
                $(this).addClass('animated');
            }, { offset: '65%' });

            var $submenu = $('.submenu');

            $submenu.affix({
                offset: { top: 800 }
            });

        });

    }]);

