'use strict';

angular.module('geanApp')
    .controller('FinderCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', 'classificacaoService', 'faqService', 'featureService', '_', function ($scope, $rootScope, $stateParams, $timeout, classificacaoService, faqService, featureService, _) {

        $scope.eanNumber = $stateParams.ean;

        $scope.beforeSearch = function () {
            // Models
            $scope.ean = null;
            $scope.feature = null;
            $scope.solicitacaoEan = {};

            // Message
            $scope.message = null;
            $scope.messageClass = null;
            $scope.confirmationMessage = null;
            $scope.faqMessageHearder = 'Ainda não há dúvidas cadastradas para este EAN.';

            // Boolean controls
            $scope.showSolicitacaoEan = false;
            $scope.solicitacaoExecuted = false;
            $scope.solicitacaoEmailDisabled = false;
            $scope.showFaq = false;
            $scope.faqCreated = false;
            $scope.showVote = false;
            $scope.showQuestionField = false;
            $scope.showSugestionField = false;
            $scope.sugested = false;
            $scope.liked = false;
            $scope.disliked = false;
            $scope.likedOrDisliked = false;

            // Elements
            // Elemento é necessário para poder realizar uso de focus
            $scope.faqQuestionInput = angular.element(document.querySelector( '#faq_question' ));
        };

        $scope.beforeSearch();

        $scope.userQuestion = {
        };

        $scope.showFaqs = function() {
            $scope.showVote = false;
            $scope.showFaq = true;
        };

        $scope.showVotes = function() {
            $scope.showFaq = false;
            $scope.showVote = true;
        };

        $scope.search = function () {
            if ($scope.eanNumber) {
                var eanPromise = classificacaoService.search($scope.eanNumber);

                eanPromise.then(function(ean){
                    $scope.ean = ean;

                    if (!$scope.ean) {
                        $scope.message = 'Esse ean ainda não consta na nossa base de dados. Deixe seu e-mail e a descrição da mercadoria para classificarmos e lhe notificaremos assim que possível!';
                        $scope.messageClass = 'warning';
                        $scope.showSolicitacaoEan = true;

                        if(Boolean($rootScope.user)){
                            $scope.solicitacaoEan.email = $rootScope.user.email;
                            $scope.solicitacaoEmailDisabled = true;
                        }
                    } else {
                        $scope.userQuestion.ean =  $scope.ean.ean;
                        $scope.showVotes();
                        $scope.showSugestion();

                        if($scope.ean.faqs.length > 0) {
                            $scope.faqMessageHearder = 'Veja as dúvidas que outros usuários tiveram sobre essa NCM.';
                        }
                    }
                });
            } else {
                $scope.ean = null;
                $scope.message = 'Digite o número do EAN';
                $scope.messageClass = 'danger';
            }
        };

        $scope.search();

        $scope.hideMessage = function() {
            $scope.message = null;
        };

        $scope.like = function (ean) {
            if($rootScope.user) {
                classificacaoService.like(ean).then(function () {
                    $scope.liked = true;
                    $scope.likedOrDisliked = true;
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.dislike = function (ean) {
            if($rootScope.user) {
                classificacaoService.dislike(ean).then(function () {
                    $scope.disliked = true;
                    $scope.likedOrDisliked = true;
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.agreeWithAnswer = function(id) {
            if($rootScope.user) {
                faqService.agreeWithAnswer(id).then(function () {
                    var faqReferenciado = _.find($scope.ean.faqs, function(faq){
                        return faq.id === id;
                    });

                    faqReferenciado.agreed = true;
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.showQuestion = function() {
            if($rootScope.user) {
                $scope.showQuestionField = true;
                $timeout(function(){
                    $scope.faqQuestionInput.focus();
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.showSugestion = function() {
            $scope.showSugestionField = true;
        };

        $scope.sendQuestion = function(userQuestion) {
            faqService.newQuestion(userQuestion).then(function(faq){
                $scope.showQuestionField = false;
                $scope.faqCreated = true;
                $scope.ean.faqs.push(faq);
            });
        };

        $scope.voteInFeature = function(feature) {
            if($rootScope.user) {
                featureService.vote(feature.id).then(function() {
                    var funcionalidadeVotada = _.find($scope.ean.funcionalidades, function(funcionalidade){
                        return funcionalidade.id === feature.id;
                    });

                    funcionalidadeVotada.voted = true;
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.sendRequestFeature = function(userFeature) {
            if($rootScope.user) {
                featureService.requestFeature(userFeature).then(function() {
                    $scope.sugested = true;
                    $scope.showSugestionField = false;
                });
            } else {
                $('#loginModal').modal('show');
            }
        };

        $scope.requestEan = function(eanRequest) {
            eanRequest.ean = $scope.eanNumber;
            classificacaoService.requestEan(eanRequest).then(function(response) {
                $scope.solicitacaoExecuted = true;
            });
        };

    }]);
