'use strict';

angular.module('geanApp')
    .controller('LoginCtrl', ['$scope', '$rootScope', '$state', '$cookieStore', 'userService', function ($scope, $rootScope, $state, $cookieStore, userService) {

        var loginWindow = $('#loginModal');
        $scope.credentials = {};

        $scope.login = function(credentials) {
            userService.authenticate(credentials).then(function(data){
                $cookieStore.put('securityToken', data);

                $rootScope.authToken = data.token;
                $rootScope.user = data.user;
                loginWindow.modal('hide');
            });
        };

        $scope.goToRegister = function() {
            $state.go('cadastro');
            loginWindow.modal('hide');
        };

    }]);
