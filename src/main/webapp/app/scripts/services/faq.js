'use strict';

/**
 * @name faqService
 * @description Serviço para manipular as ações no FAQ
 */
angular.module('geanApp')
    .service('faqService', ['$http', '$q', function ($http, $q) {

        /**
         * Método para gravar que
         * @param id
         * @returns promise
         */
        this.agreeWithAnswer = function(id) {
            var d = $q.defer();
            $http.post('faq/like/' + id).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };


        /**
         * Método para salvar uma nova pergunta
         * @param question
         * @returns promise
         */
        this.newQuestion = function (question) {
            var d = $q.defer();
            $http.post('faq/create', question).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };


    }]);

