'use strict';

/**
 * @name featureService
 * @description Serviço para controlar as features
 */
angular.module('geanApp')
    .service('featureService', ['$http', '$q', function ($http, $q) {

        /**
         * Método para gravar um voto
         * @param id da funcionalidade
         * @returns promise
         */
        this.vote = function(id) {
            var d = $q.defer();
            $http.post('funcionalidade/votar/' + id).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };

        /**
         * Método para solicitar uma nova funcionalidade
         * @param funcionalidade
         * @returns promise
         */
        this.requestFeature = function(feature) {
            var d = $q.defer();
            $http.post('funcionalidade', feature).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };


    }]);