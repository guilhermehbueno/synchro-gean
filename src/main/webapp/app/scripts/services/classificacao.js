'use strict';

/**
 * @name classificacaoService
 * @description Serviço para consultar classificações
 */
angular.module('geanApp')
    .service('classificacaoService', ['$filter', '$http', '$q', function ($filter, $http, $q) {

        /**
         * Método para fazer uma consulta através de um EAN
         * @param ean
         * @returns promise
         */
        this.search = function(ean) {
            var d = $q.defer();
            $http.get('classificacao/' + ean).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };

        /**
         * Método para que marca o EAN como like
         * @param ean
         * @return promise
         */
        this.like = function(ean) {
            return execute(ean);
        };

        /**
         * Método para que marca o EAN como dislike
         * @param ean
         * @return promise
         */
        this.dislike = function(ean) {
            return execute(ean, 'dislike');
        };

        /**
         * Método privado que faz a post para o servidor, passando o EAN e o tipo de estatística.
         * @param ean
         * @param type
         * @return promise
         */
        var execute = function(ean, type) {
            var d = $q.defer();
            var params = {};

            if(type) {
                params.not = true;
            }

            $http.post('classificacao/like/' + ean.id , params).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };

        this.requestEan = function(eanRequest){
            var d = $q.defer();

            debugger;
            $http.post('classificacao/solicitacaoEan' , eanRequest).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };

    }]);