'use strict';

/**
 * @name userService
 * @description Módulo responsável pelo cadastro e manutenção do usuário
 */
angular.module('geanApp')
    .service('userService', ['$http', '$q', function ($http, $q) {

        /**
         * Método para cadastrar um novo usuário
         * @param user
         * @returns promise
         */
        this.create = function(user) {
            var d = $q.defer();
            $http.post('user', user).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };


        this.authenticate = function(user){
            var d = $q.defer();
            $http.post('authentication', user).success(function(response) {
                d.resolve(response);
            });
            return d.promise;
        };


    }]);