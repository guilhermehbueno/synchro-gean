'use strict';

describe('Controller: MainCtrl', function () {

    // load the controller's module
    beforeEach(module('geanApp'));

    var $scope, $location, $rootScope, createController;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($injector) {
        $location = $injector.get('$location');
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();

        var $controller = $injector.get('$controller');

        createController = function() {
            return $controller('MainCtrl', {
                '$scope': $scope
            });
        };
    }));

    it('location path shoud be a root', function () {
        createController();
        $location.path('/');
        expect($location.path()).toBe('/');
    });

});
