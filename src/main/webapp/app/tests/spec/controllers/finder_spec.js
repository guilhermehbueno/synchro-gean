'use strict';

describe('Controller: FinderCtrl', function () {

    beforeEach(module('geanApp'));
    var $scope, $rootScope, createController,
        ean = {
            id: 4,
            ean: '9876',
            ncm: '8744.01.87',
            descricao: 'Mouse Optico HP',
            faqs: [
                {
                    id: 12,
                    pergunta: 'Meu mouse é analógico',
                    resposta: 'Mouses analógicos não possui o mesmo NCM'
                }
            ],
            funcionalidades: [
                {
                    id: 10,
                    description: 'Foto do Produto',
                    quantidadeVotos: 1
                },
                {
                    id: 21,
                    description: 'Descrição detalhada do produto',
                    quantidadeVotos: 0
                }
            ]
        };

    beforeEach(inject(function ($injector) {

        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();

        var $controller = $injector.get('$controller');

        createController = function() {
            return $controller('FinderCtrl', {
                '$scope': $scope
            });
        };
    }));

    it('should reset variables before a search', function () {
        createController();

        $scope.message = 'My Message';
        $scope.ean = 'EAN';
        $scope.messageClass = 'MCLASS';
        $scope.confirmationMessage = 'Confirm';
        $scope.showFaq = true;
        $scope.showVote = true;
        $scope.showQuestionField = true;
        $scope.showSugestionField = true;
        $scope.liked = true;
        $scope.disliked = true;
        $scope.feature = 'Feature';
        $scope.voted = true;

        $scope.beforeSearch();

        expect($scope.message).toBeNull();
        expect($scope.ean).toBeNull();
        expect($scope.messageClass).toBeNull();
        expect($scope.confirmationMessage).toBeNull();
        expect($scope.showFaq).toBeFalsy();
        expect($scope.showVote).toBeFalsy();
        expect($scope.showQuestionField).toBeFalsy();
        expect($scope.showSugestionField).toBeFalsy();
        expect($scope.liked).toBeFalsy();
        expect($scope.disliked).toBeFalsy();
        expect($scope.feature).toBeNull();
        expect($scope.voted).toBeFalsy();

    });


    it('should set showFaqs', function () {
        createController();
        $scope.showVote = true;
        $scope.showFaq = false;

        $scope.showFaqs();

        expect($scope.showFaq).toBeTruthy();
        expect($scope.showVote).toBeFalsy();
    });

    it('should set showVotes', function () {
        createController();
        $scope.showVote = false;
        $scope.showFaq = true;

        $scope.showVotes();

        expect($scope.showFaq).toBeFalsy();
        expect($scope.showVote).toBeTruthy();
    });

    it('should hide a message', function () {
        createController();
        $scope.message = 'My message';

        $scope.hideMessage();

        expect($scope.message).toBeNull();
    });

    it('Should show question', function(){
        createController();
        $scope.showQuestionField = false;
        $rootScope.user = {userName: 'Teste'};
        $scope.showQuestion();
        expect($scope.showQuestionField).toBeTruthy();
    });

    it('Should show Sugestion', function(){
        createController();
        $scope.showSugestionField = false;
        $rootScope.user = {userName: 'Teste'};
        $scope.showSugestion();
        expect($scope.showSugestionField).toBeTruthy();
    });



    it('should search an ean', function () {
        createController();

        var classificacaoService = {
            query: function() {}
        };

        var ncm = '8744.01.87', eanNumber = '9876';

        $scope.eanNumber = eanNumber;

        $scope.beforeSearch();
        spyOn(classificacaoService, 'query').andReturn(ean);
        $scope.search();

        expect($scope.ean.ncm).toBe(ncm);

    });


    it('should search not find an ean', function () {
        createController();

        var classificacaoService = {
            query: function() {}
        };

        var ncm = null, eanNumber = '000';

        $scope.eanNumber = eanNumber;

        $scope.beforeSearch();
        spyOn(classificacaoService, 'query').andReturn(ncm);
        $scope.search();

        expect($scope.message).toBe('Número não encontrado');
        expect($scope.messageClass).toBe('warning');
    });

    describe('Like and Dislike Functions', function () {
        var $scope, $rootScope, classificacaoService, $q, deferred;

        beforeEach(inject(function ($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            $q = $injector.get('$q');

            var $controller = $injector.get('$controller');
            createController = function() {
                return $controller('FinderCtrl', {
                    '$scope': $scope,
                    'classificacaoService': classificacaoService,
                    '$q': $q
                });
            };
        }));

        beforeEach(function() {
            $rootScope.user = {userName: 'Teste'};
            classificacaoService = {
                like: function() {
                    deferred = $q.defer();
                    return deferred.promise;
                },
                dislike: function() {
                    deferred = $q.defer();
                    return deferred.promise;
                }
            };
        });

        it('Should like a classificacao', function(){
            createController();
            spyOn(classificacaoService, 'like').andCallThrough();
            $scope.like('12');
            deferred.resolve();
            $rootScope.$digest();
            expect(classificacaoService.like).toHaveBeenCalled();
            expect($scope.liked).toBeTruthy();
        });


        it('Should like a classificacao', function(){
            createController();
            spyOn(classificacaoService, 'dislike').andCallThrough();
            $scope.dislike('12');
            deferred.resolve();
            $rootScope.$digest();
            expect(classificacaoService.dislike).toHaveBeenCalled();
            expect($scope.disliked).toBeTruthy();
        });

    });

    describe('Send question and agree with amswer', function () {
        var $scope, $rootScope, faqService, $q, deferred, $compile;

        beforeEach(inject(function ($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            $q = $injector.get('$q');
            $compile = $injector.get('$compile');

            var $controller = $injector.get('$controller');
            createController = function() {
                return $controller('FinderCtrl', {
                    '$scope': $scope,
                    'faqService': faqService,
                    '$q': $q
                });
            };
        }));

        beforeEach(function() {
            $rootScope.user = {userName: 'Teste'};
            faqService = {
                agreeWithAnswer: function() {
                    deferred = $q.defer();
                    return deferred.promise;
                },
                newQuestion: function() {
                    deferred = $q.defer();
                    return deferred.promise;
                }
            };
        });

        it('Should agree with an answer', function(){
            createController();
            $compile('<button id="faq_12"></button>')($scope);

            spyOn(faqService, 'agreeWithAnswer').andCallThrough();
            $scope.agreeWithAnswer('12');
            deferred.resolve();
            expect(faqService.agreeWithAnswer).toHaveBeenCalled();
        });


        it('Should send a new question', function(){
            createController();
            spyOn(faqService, 'newQuestion').andCallThrough();
            $scope.sendQuestion({});
            deferred.resolve();
            expect(faqService.newQuestion).toHaveBeenCalled();
        });

    });

});
