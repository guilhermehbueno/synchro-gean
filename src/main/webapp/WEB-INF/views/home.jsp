<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>ClassifiquEan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link href="dist/stylesheets/main.css" rel="stylesheet">

    <link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

</head>
<body ng-app="geanApp">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div ng-include="'app/views/includes/modal-login.html'"></div>
<header id="header" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a ui-sref="home" class="navbar-brand"><img src="dist/images/logo.png" alt="" /></a>
        </div>

        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ui-view>
                <div ui-view="menu"></div>
            </ui-view>


            <ul class="nav navbar-nav navbar-right hidden-sm">
                <li ng-show="$root.user">
                    <button type="button" class="btn btn-default">Ol&aacute;, {{$root.user.username}}</button>
                </li>
                <li ng-hide="$root.user">
                    <button type="button" class="btn btn-primary" ng-controller="MainCtrl" ng-click="showLogin();">Entrar</button>
                </li>
            </ul>
        </nav>

    </div>
</header>

<!-- CONTENT WRAPPER
============================================= -->

<ui-view main>
    <div id="content-wrapper" ui-view="main"></div>
</ui-view>

<script src="dist/prod.min.js" type="text/javascript"></script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52605006-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
