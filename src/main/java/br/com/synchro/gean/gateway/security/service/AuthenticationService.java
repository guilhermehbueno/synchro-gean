package br.com.synchro.gean.gateway.security.service;

import br.com.synchro.gean.business.service.UserService;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Service
public class AuthenticationService {


    /**
     * Referencia para servico de usuarios.
     */
    @Autowired
    private UserService userService;

    /**
     * Referencia para o servico de SecurityToken.
     */
    @Autowired
    private SecurityTokenService securityTokenService;

    /**
     * Referencia para o Authentication Manager do Spring, responsavel por verificar a autenticidade das credenciais do usuario.
     */
    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;


    /**
     * @param user
     * @return
     */
    public Map<String, Object> authenticate(GeanUser user) {
        Map<String, Object> mappedToken = null;
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        GeanUser loadedUser = userService.getUserByEmail(user.getEmail());
        if (loadedUser != null) {
            SecurityToken token = securityTokenService.createAndAdd(loadedUser);
            mappedToken = mapSecurityToken(token);
        }

        return mappedToken;
    }

    private Map<String, Object> mapSecurityToken(SecurityToken token) {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("token", token.getToken());
        map.put("expiresIn", token.getExpiresIn());

        HashMap<String, Object> user = new HashMap<String, Object>();
        user.put("username", token.getUser().getUsername());
        user.put("email", token.getUser().getEmail());
        user.put("company", token.getUser().getCompany());
        user.put("roles", token.getUser().getRoles());
        map.put("user", user);

        return map;
    }
}
