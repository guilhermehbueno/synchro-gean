package br.com.synchro.gean.gateway.persistence.mapping;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Representacao de um Usuario registrado no sistema.
 *
 * @author Eduardo Froes (eduardo.froes@synchro.com.br)
 */
@Entity(name = "cfm_user")
public class GeanUser implements java.io.Serializable, Cloneable {

    /**
     * Serial Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Nome.
     */
    @NotNull
    @Size(min = 4)
    private String username;

    /**
     * Usuario.
     */
    @NotNull
    @Size(min = 8)
    @Column(unique = true)
    private String email;

    /**
     * Nome da empresa na qual este usuario trabalhha.
     */
    private String company;

    /**
     * Senha (cripto).
     */
    @NotNull
    @Size(min = 1)
    private String password;

    /**
     * Papeis do usuario
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "cfm_user_roles", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role", nullable = false)
    private List<String> roles;

    /**
     * Construtor padrao.
     */
    public GeanUser() {

    }

    /**
     * Construtor configura nome, email, empresa, senha.
     *
     * @param username Nome.
     * @param email    Email.
     * @param company  Empresa.
     * @param password Senha.
     */
    public GeanUser(String username, String password, String email, String company) {
        this.setUsername(username);
        this.setEmail(email);
        this.setCompany(company);
        this.setPassword(password);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    /**
     * Cria um clone, com o estado atual, deste objeto.
     *
     * @return Instancia clonada.
     */
    @Override
    public GeanUser clone() {
        GeanUser cloned = new GeanUser();
        cloned.setId(this.id);
        cloned.setUsername(this.username);
        cloned.setEmail(this.email);
        cloned.setCompany(this.company);
        cloned.setPassword(this.password);
        cloned.setRoles(this.roles);
        return cloned;
    }
}
