package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Representa a classificacao (codigo NCM) de um EAN. Ou seja, para um dado EAN, relaciona sua NCM e Descricao.
 *
 * @author Eduardo Froes (eduardo.froes@synchro.com.br)
 */
@Entity(name = "cfm_classificacao_ean")
public class ClassificacaoEan implements java.io.Serializable {

    /**
     * Serial Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Codigo EAN.
     */
    @Size(min = 8)
    @NotNull
    @Column(nullable = false)
    private String ean;

    /**
     * Codigo NCM.
     */
    @Size(min = 4)
    @NotNull
    @Column(nullable = false)
    private String ncm;

    /**
     * Descricao da NCM.
     */
    @NotNull
    @Size(min = 3, max = 2000)
    @Column(nullable = false)
    private String description;

    /**
     * Construtor padrao.
     */
    public ClassificacaoEan() {

    }

    /**
     * Construtor configura ean, ncm, descricao.
     *
     * @param ean
     * @param ncm
     * @param description
     */
    public ClassificacaoEan(String ean, String ncm, String description) {
        this.ean = ean;
        this.ncm = ncm;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getNcm() {
        return ncm;
    }

    public void setNcm(String ncm) {
        this.ncm = ncm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(getEan());
        sb.append(" -> ");
        sb.append(getNcm());
        sb.append(" - ");
        sb.append(getDescription());
        return sb.toString();
    }
}
