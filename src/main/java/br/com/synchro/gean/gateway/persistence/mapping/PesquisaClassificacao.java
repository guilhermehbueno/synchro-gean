package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Registro de uma pesquisa por classificacao feita por um usuario.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Entity(name = "cfm_pesquisa_classificacao")
public class PesquisaClassificacao {

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date dateCreated;

    @NotNull
    @Column(nullable = false)
    private String ean;

    @Column
    private Boolean encontrou;

    /**
     * @param ean
     * @param encontrou
     */
    public PesquisaClassificacao(String ean, Boolean encontrou) {
        this.ean = ean;
        this.encontrou = encontrou;
    }

    public PesquisaClassificacao(){

    }


    /**
     * Configura a data de criacao.
     */
    @PrePersist
    public void prePersist() {
        setDateCreated(new Date());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public Boolean getEncontrou() {
        return encontrou;
    }

    public void setEncontrou(Boolean encontrou) {
        this.encontrou = encontrou;
    }
}
