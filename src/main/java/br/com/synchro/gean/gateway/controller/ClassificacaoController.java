package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.business.service.*;
import br.com.synchro.gean.gateway.controller.model.SolicitacaoEanTO;
import br.com.synchro.gean.gateway.persistence.mapping.*;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.*;

/**
 * Prove interface de acesso HTTP ao servico de pesquisa de Classificação por Ean.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Controller
@RequestMapping("/classificacao")
public class ClassificacaoController {

    /**
     * Referencia para servico de classficacao.
     */
    @Autowired
    private ClassificacaoEanService classificacaoEanService;

    /**
     * Referencia para servico Faq.
     */
    @Autowired
    private FaqService faqService;

    /**
     * Referencia para servico de funcionalidade.
     */
    @Autowired
    private FuncionalidadeService funcionalidadeService;

    /**
     * Referencia para servico de votos.
     */
    @Autowired
    private VotoService votoService;

    /**
     * Referencia para servico de usuarios.
     */
    @Autowired
    private UserService userService;


    /**
     * @param ean
     * @return
     */
    @RequestMapping(value = "/{ean}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> findClassificacaoByEan(@PathVariable("ean") String ean) {
        HashMap<String, Object> responseBody = new HashMap<String, Object>();

        ClassificacaoEan classificacaoEan = classificacaoEanService.findByEan(ean);
        if (classificacaoEan != null) {
            responseBody.put("id", classificacaoEan.getId());
            responseBody.put("ean", classificacaoEan.getEan());
            responseBody.put("ncm", classificacaoEan.getNcm());
            responseBody.put("descricao", classificacaoEan.getDescription());

            List<Faq> faqs = faqService.findByEanAndAnswerIsNotNull(ean);
            if (faqs != null && !faqs.isEmpty()) {
                responseBody.put("faqs", faqs);
            }else{
                responseBody.put("faqs", new ArrayList<Faq>());
            }

            List<Funcionalidade> emDestaque = funcionalidadeService.findFuncionalidadesEmDestaque();
            if (emDestaque != null && !emDestaque.isEmpty()) {
                responseBody.put("funcionalidades", ImmutableSet.copyOf(emDestaque).asList());
            }
        } else {
            return new ResponseEntity<Map<String, Object>>(responseBody, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Map<String, Object>>(responseBody, HttpStatus.OK);
    }

    /**
     * Registra um like de um usuario para uma classificacao.
     *
     * @param id ID da classificacao.
     * @return 201 em caso de sucesso, 400 caso contrario.
     */
    @RequestMapping(value = "/like/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity like(@RequestParam(value = "not", required = false) String not, @PathVariable("id") Long id) {
        GeanUser user = (GeanUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        votoService.votar(new Voto(user, Voto.TipoInformacao.CLASSIFICACAO, id, StringUtils.isEmpty(not)));
        return new ResponseEntity(HttpStatus.CREATED);
    }


    /**
     * @param file
     * @return
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST)
    public ResponseEntity load(@RequestParam("file") MultipartFile file) {
        try {
            classificacaoEanService.load(file.getInputStream());
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.CREATED);

    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity count(){
        return new ResponseEntity(classificacaoEanService.count(), HttpStatus.OK);
    }

    /**
     * Registra uma solicitação de um ean que ainda não foi cadastrado.
     */
    @RequestMapping(value = "/solicitacaoEan", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity solicitacaoEan(@Valid @RequestBody final SolicitacaoEanTO solicitacaoTO) {
        classificacaoEanService.addSolicitacaoEan(new SolicitacaoEan(
                solicitacaoTO.getEmail(), solicitacaoTO.getEan(), solicitacaoTO.getMercadoria()
        ));
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
