package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.SolicitacaoEan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * <pre>
 * User: rfh
 * Date: 10/21/14
 *
 * Repositório para busca e persistência de solicitações de ean
 *
 * </pre>
 */
public interface SolicitacaoEanRepository extends JpaRepository<SolicitacaoEan, Long> {

}
