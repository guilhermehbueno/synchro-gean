package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Representa um pedido de funcionalidade feito por um determinado usuario.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Entity(name = "cfm_pedido_funcionalidade")
public class Funcionalidade implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    /**
     * Descricao desta feature.
     */
    @NotNull
    @Size(min = 10)
    @Column(nullable = false)
    private String description;

    /**
     * Usuario que primeiro registrou este pedido de feature.
     */
    @OneToOne
    @JoinColumn(nullable = false)
    @NotNull
    private GeanUser owner;

    @Transient
    private Long quantidadeVotos;

    /**
     * Timestamp de criacao desta funcionalidade.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date dateCreated;

    /**
     * Construtor padrao.
     */
    public Funcionalidade() {
    }

    /**
     * Construtor copia os valores da Funcionalidade <code>f</code> e a quantidade de votos recebidos.
     *
     * @param f        Funcionalidade original.
     * @param qtdVotos Quantidade de votos.
     */
    public Funcionalidade(Funcionalidade f, Long qtdVotos) {
        setId(f.getId());
        setDescription(f.getDescription());
        setOwner(f.getOwner());
        setDateCreated(f.getDateCreated());
        setQuantidadeVotos(qtdVotos);
    }

    /**
     * Construtor configura a descricao e o usuario "dono" desta feature request.
     *
     * @param description Descricao da feature.
     * @param owner       Usuario primeiro registrou essa feature.
     */
    public Funcionalidade(String description, GeanUser owner) {
        this.description = description;
        this.owner = owner;
    }

    /**
     * Configura a data de criacao.
     */
    @PrePersist
    public void prePersist() {
        setDateCreated(new Date());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GeanUser getOwner() {
        return owner;
    }

    public void setOwner(GeanUser owner) {
        this.owner = owner;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getQuantidadeVotos() {
        return quantidadeVotos;
    }

    public void setQuantidadeVotos(Long quantidadeVotos) {
        this.quantidadeVotos = quantidadeVotos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Funcionalidade that = (Funcionalidade) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        return result;
    }
}
