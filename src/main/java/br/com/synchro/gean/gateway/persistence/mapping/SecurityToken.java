package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.*;
import java.util.List;

/**
 * Representa um token que permite acesso a um dado usuario.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Entity(name = "cfm_security_token")
public class SecurityToken {

    /**
     * Id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Chave criptografada que garante a unicidade do token.
     */
    @Column(nullable = false)
    private String token;

    /**
     * Usuario dono deste token.
     */
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "ID")
    private GeanUser user;

    /**
     * Data de expiracao deste token.
     */
    @Column(nullable = false)
    private Long expiresIn;

    /**
     * Construtor default.
     */
    public SecurityToken() {

    }

    /**
     * Construtor configura usuario dono deste token.
     *
     * @param user Dono do token.
     */
    public SecurityToken(GeanUser user, Long expiresIn) {
        setUser(user);
        setExpiresIn(System.currentTimeMillis() + expiresIn);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getRoles() {
        return user.getRoles();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GeanUser getUser() {
        return user;
    }

    public void setUser(GeanUser user) {
        this.user = user;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }
}
