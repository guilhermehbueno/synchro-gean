package br.com.synchro.gean.gateway.controller.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <pre>
 * User: rfh
 * Date: 10/21/14
 *
 * TO que representa uma solicitação de classificação de uma mercadoria para um ean ainda não cadastrado no sistema.
 * Consiste do e-mail do usuário solicitante e a descrição da mercadoria.
 *
 * </pre>
 */
public class SolicitacaoEanTO implements Serializable {

    /**
     * E-mail do usuário solicitante
     */
    @NotNull
    @Size(min = 8)
    private String email;

    /**
     * Ean ainda sem ncm associada
     */
    @NotNull
    private String ean;

    /**
     * Mercadoria pendente de classificação
     */
    @NotNull
    @Size(min = 3, max = 2000)
    private String mercadoria;

    public SolicitacaoEanTO() {}

    public SolicitacaoEanTO(final String email, final String ean, final String mercadoria) {
        this.email = email;
        this.ean = ean;
        this.mercadoria = mercadoria;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(final String ean) {
        this.ean = ean;
    }

    public String getMercadoria() {
        return mercadoria;
    }

    public void setMercadoria(final String mercadoria) {
        this.mercadoria = mercadoria;
    }
}
