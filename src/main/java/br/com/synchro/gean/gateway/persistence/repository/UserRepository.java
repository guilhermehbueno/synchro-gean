package br.com.synchro.gean.gateway.persistence.repository;


import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interface de acesso aos dados de usuarios.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.GeanUser
 */
public interface UserRepository extends JpaRepository<GeanUser, Integer> {

    /**
     * Obtem um usuario dado seu email.
     *
     * @param strEmail Email do usuario em questao
     * @return Usuario correspondente,
     */
    @Transactional(readOnly = true)
    public GeanUser findByEmail(String strEmail);

}
