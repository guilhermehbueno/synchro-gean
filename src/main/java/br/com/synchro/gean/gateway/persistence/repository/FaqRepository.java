package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface de acesso aos dados de Faq.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.Faq
 */
public interface FaqRepository extends JpaRepository<Faq, Long> {

    /**
     * Obtem as perguntas/respostas registradas para um dado EAN.
     *
     * @param ean Codigo do EAN
     * @return Faqs correspondentes ao EAN dado.
     */
    List<Faq> findByEan(String ean);

    List<Faq> findByEanAndAnswerIsNotNull(String ean);

}
