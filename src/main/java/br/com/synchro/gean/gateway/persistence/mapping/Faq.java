package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * Representa as perguntas/respostas registradas para um dado EAN.
 *
 * @author Eduardo Froes (eduardo.froes@synchro.com.br)
 */
@Entity(name = "cfm_faq")
public class Faq implements java.io.Serializable {

    /**
     * Serial Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Codigo Ean.
     */
    @Size(min = 8)
    @Column(nullable = false)
    private String ean;

    /**
     * Pergunta.
     */
    @Column(length = 3000, nullable = true)
    private String question;

    /**
     * Resposta.
     */
    @Column(length = 3000)
    private String answer;

    /**
     * Construtor padrao.
     */
    public Faq() {

    }

    /**
     * Construtor configura ean, pergunta e resposta.
     *
     * @param ean      Codigo Ean.
     * @param question Pergunta.
     * @param answer   Resposta.
     */
    public Faq(String ean, String question, String answer) {
        this.ean = ean;
        this.question = question;
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
