package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.business.service.FaqService;
import br.com.synchro.gean.business.service.VotoService;
import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Prove os controles necessarios para interacao com mecanismo de apresentacao.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Controller
@RequestMapping(value = "/faq")
public class FaqController {

    /**
     * Referencia para servico de Faq.
     */
    @Autowired
    private FaqService faqService;

    /**
     * Referencia para servico de votacao.
     */
    @Autowired
    private VotoService votoService;

    /**
     * Realiza o registro de uma nova pergunta ao sistema de FAQ.
     *
     * @param faq Instancia representando a pergunta a ser criada.
     * @return Objeto representando a resposta (body + status).
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Faq> registrarFaq(@RequestBody Faq faq) {
        Faq registeredFaq = faqService.addFaq(faq);
        return new ResponseEntity<Faq>(registeredFaq, HttpStatus.CREATED);
    }

    /**
     * Registra um like de um usuario para uma faq.
     *
     * @param id ID da faq.
     * @return 201 em caso de sucesso, 400 caso contrario.
     */
    @RequestMapping(value = "/like/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity like(@PathVariable("id") Long id) {
        GeanUser user = (GeanUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        votoService.votar(new Voto(user, Voto.TipoInformacao.FAQ, id, true));
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
