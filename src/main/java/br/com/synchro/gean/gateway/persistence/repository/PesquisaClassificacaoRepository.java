package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.PesquisaClassificacao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public interface PesquisaClassificacaoRepository extends JpaRepository<PesquisaClassificacao, Long> {

    /**
     * @param ean
     * @return
     */
    public List<PesquisaClassificacao> findByEan(String ean);

}
