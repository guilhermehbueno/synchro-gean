package br.com.synchro.gean.gateway.security.spring;

import br.com.synchro.gean.business.service.UserService;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.security.service.AuthenticationService;
import br.com.synchro.gean.gateway.security.service.SecurityTokenService;
import br.com.synchro.gean.gateway.serialization.SerializationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Component
public class AuthenticationTokenProcessingFilter extends OncePerRequestFilter {

    /**
     * Referencia para servico de validacao do token.
     */
    @Autowired
    private SecurityTokenService securityTokenService;

    /**
     * User details service.
     */
    @Autowired
    private GeanUserDetailsService geanUserDetailsService;

    /**
     * User service.
     */
    @Autowired
    private UserService userService;

    /**
     * Serialization service
     */
    @Autowired
    private SerializationService serializationService;

    @Override
    protected void doFilterInternal(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse, final FilterChain filterChain) throws ServletException, IOException {
        String authToken = httpRequest.getHeader("Authorization");
        String email = httpRequest.getHeader("Subject");

        if(authToken != null && !authToken.equals("") && email != null && !email.equals("")){
            final boolean isExpired = securityTokenService.isExpired(authToken);

            if(isExpired){
                final GeanUser user = userService.getUserByEmail(email);
                authenticate(user);
                final SecurityToken securityToken = securityTokenService.createAndAdd(user);
                httpResponse.addHeader("new-security-token", serializationService.serialize(mapSecurityToken(securityToken)));

            }else{
                SecurityToken securityToken = securityTokenService.loadSecurityToken(authToken);
                authenticate(securityToken.getUser());
            }
        }

        filterChain.doFilter(httpRequest, httpResponse);
    }

    /**
     * @param user
     * @throws Exception
     */
    private void authenticate(GeanUser user) {
        List<GrantedAuthority> authorities = geanUserDetailsService.getGrantedAuthorities(user);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
        authentication.setDetails(geanUserDetailsService.loadUserByUsername(user.getEmail()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


    /**
     * @param request
     * @return
     */
    private HttpServletRequest getAsHttpRequest(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }
        return (HttpServletRequest) request;
    }

    private Map<String, Object> mapSecurityToken(SecurityToken token) {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("token", token.getToken());
        map.put("expiresIn", token.getExpiresIn());

        HashMap<String, Object> user = new HashMap<String, Object>();
        user.put("username", token.getUser().getUsername());
        user.put("email", token.getUser().getEmail());
        user.put("company", token.getUser().getCompany());
        user.put("roles", token.getUser().getRoles());
        map.put("user", user);

        return map;
    }

}
