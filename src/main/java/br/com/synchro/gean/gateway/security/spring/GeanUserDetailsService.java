package br.com.synchro.gean.gateway.security.spring;

import br.com.synchro.gean.business.service.UserService;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementacao de {@link org.springframework.security.core.userdetails.UserDetailsService} responsavel por carregar
 * o usuario Gean ({@link br.com.synchro.gean.gateway.persistence.mapping.GeanUser}) e encapsula-lo em um {@link org.springframework.security.core.userdetails.UserDetails}.
 * <p/>
 * Portanto, essa implementacao existe para satisfazer essa necessidade do Spring Security.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see org.springframework.security.core.userdetails.UserDetailsService
 */
@Service
public class GeanUserDetailsService implements UserDetailsService {

    /**
     * Referencia para servico de usuarios.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Carrega o usuario do Gean correspondente ao email dado e encapsula em um UserDetails.
     *
     * @param userEmail Email do usuario em questao.
     * @return UserDetails para o usuario gean.
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        User userDetail = null;

        GeanUser geanUser = userRepository.findByEmail(userEmail);

        if (geanUser != null) {
            userDetail = new User(geanUser.getEmail(), geanUser.getPassword(), getGrantedAuthorities(geanUser));
        } else {
            throw new UsernameNotFoundException("Usuario [" + userEmail + "] nao encontrado.");
        }

        return userDetail;
    }

    /**
     * Com base nos papeis do usuario, retorna uma lista de GrantedAuthority. Ou seja, encapsula os papeis
     * do usuario neste objeto conforme requerido pelo Spring Security.
     *
     * @param geanUser Usuario cujo papeis devem ser convertidos em GrantedAuthority.
     * @return Uma lista contendo os papeis do usuario encapsulados na forma de GrantedAuthority.
     * @see org.springframework.security.core.GrantedAuthority
     */
    public List<GrantedAuthority> getGrantedAuthorities(GeanUser geanUser) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        if (geanUser.getRoles() != null && !geanUser.getRoles().isEmpty()) {
            for (String role : geanUser.getRoles()) {
                authorities.add(new SimpleGrantedAuthority(role));
            }
        }
        return authorities;
    }
}
