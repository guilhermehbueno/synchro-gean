package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Interface para acesso a dados de Classificacao dos Eans.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan
 */
public interface ClassificacaoEanRepository extends JpaRepository<ClassificacaoEan, Long> {


    /**
     * Obtem a classificacao para um dado codigo EAN.
     *
     * @param sEAN Codigo EAN para o qual se deseja a classificacao.
     * @return Classificacao correspondente ao EAN dado.
     */
    public ClassificacaoEan findByEan(String sEAN);

}
