package br.com.synchro.gean.gateway.serialization;

import org.boon.json.JsonFactory;
import org.boon.json.ObjectMapper;
import org.springframework.stereotype.Component;


/**
 * <pre>
 * User: rfh
 * Date: 10/8/14
 *
 * </pre>
 */
@Component
public class SerializationServiceBoon implements SerializationService {

    private final ObjectMapper objectMapper;

    SerializationServiceBoon(){
        this.objectMapper = JsonFactory.create();
    }
    
    @Override
    public String serialize(final Object object) {
        return objectMapper.toJson(object);
    }

    @Override
    public <T> T deserialize(final String source, final Class<T> target) {
        return objectMapper.fromJson(source, target);
    }
}
