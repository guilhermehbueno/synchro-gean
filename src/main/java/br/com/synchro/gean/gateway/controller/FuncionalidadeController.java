package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.business.service.FuncionalidadeService;
import br.com.synchro.gean.business.service.UserService;
import br.com.synchro.gean.business.service.VotoService;
import br.com.synchro.gean.gateway.controller.model.FuncionalidadeTO;
import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Controller
@RequestMapping("/funcionalidade")
public class FuncionalidadeController {

    /**
     * Referencia para servico de funcionalidade.
     */
    @Autowired
    private FuncionalidadeService funcionalidadeService;

    @Autowired
    private VotoService votoService;

    /**
     * Referencia para servico de usuarios.
     */
    @Autowired
    private UserService userService;

    /**
     * Registra um pedido de funcionalidade feito por um usuario.
     *
     * @param fTO Transfer Object contendo os dados do pedido de funcionalidade.
     * @return 201 caso tenha sido criado; veja {@link br.com.synchro.gean.gateway.controller.ExceptionHandlerControllerAdvice} para casos de excecao.
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addFuncionalidade(@Valid @RequestBody FuncionalidadeTO fTO) {
        GeanUser owner = (GeanUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        funcionalidadeService.addFuncionalidade(new Funcionalidade(fTO.getDescricao(), owner));
        return new ResponseEntity(HttpStatus.CREATED);
    }


    /**
     * Registra o voto de um usuario para a funcionalidade em questao.
     *
     * @param funcId ID da funcionalidade que recebeu o voto.
     * @return @return 201 caso tenha sido criado; veja {@link br.com.synchro.gean.gateway.controller.ExceptionHandlerControllerAdvice} para casos de excecao.
     */
    @RequestMapping(value = "/votar/{id}", method = RequestMethod.POST)
    public ResponseEntity registrarVoto(@PathVariable("id") Long funcId) {

        GeanUser user = (GeanUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        votoService.votar(new Voto(user, Voto.TipoInformacao.FUNCIONALIDADE, funcId, true));
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
