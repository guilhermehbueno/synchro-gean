package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public interface SecurityTokenRepository extends JpaRepository<SecurityToken, Integer> {


    @Query("SELECT s FROM br.com.synchro.gean.gateway.persistence.mapping.SecurityToken s where s.user.email = :email")
    public List<SecurityToken> findByUserEmail(@Param("email") String userEmail);

    @Query("SELECT s FROM br.com.synchro.gean.gateway.persistence.mapping.SecurityToken s where s.token = :token and s.user.email = :email")
    public SecurityToken findByTokenAndEmail(@Param("token") String token, @Param("email") String userEmail);

    public SecurityToken findByToken(String token);

}
