package br.com.synchro.gean.gateway.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Responsavel pelo controle da pagina inicial da aplicacao.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 */
@Controller
public class HomeController {

    /**
     * Abre a pagina inicial.
     *
     * @return "home" view.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome() {
        return "home";
    }

    /**
     * Rota para o favicon
     *
     * @return '/app/favicon.ico'
     */
    @RequestMapping("favicon.ico")
    String favicon() {
        return "forward:/app/favicon.ico";
    }
}
