package br.com.synchro.gean.gateway.security.service;

import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.mapping.SecurityToken;
import br.com.synchro.gean.gateway.persistence.repository.SecurityTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Reune as funcionalidades relacionadas ao SecurityToken.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.SecurityToken
 */
@Component
public class SecurityTokenService {

    /**
     * Chave usada na criptografia do token.
     */
    private static final String MAGIC_KEY = "$fenda_do_biquini";

    /**
     * Tempo de expiracao default (AGORA + 1h).
     */
    private static final Long DEFAULT_SESSION_DURATION = 1000L * 60 * 60;

    /**
     * Repositorio de SecurityToken.
     */
    @Autowired
    private SecurityTokenRepository securityTokenRepository;

    /**
     * Cria um SecurityToken para o usuario dado.
     *
     * @param user             Usuario dono do token.
     * @param durationInMillis Duracao deste token em milisegundos.
     * @return SecurityToken para o usuario dado.
     */
    public SecurityToken createSecurityToken(GeanUser user, Long durationInMillis) {
        SecurityToken securityToken = new SecurityToken(user, durationInMillis);
        securityToken.setToken(createToken(securityToken));
        return securityToken;
    }

    /**
     * Cria um SecurityToken (com duracao padrao) para o usuario dado.
     *
     * @param user Usuario dono do token.
     * @return SecurityToken para o usuario dado.
     * @see SecurityTokenService#DEFAULT_SESSION_DURATION
     */
    public SecurityToken createSecurityToken(GeanUser user) {
        return createSecurityToken(user, DEFAULT_SESSION_DURATION);
    }

    /**
     * Grava um SecurityToken no repositorio.
     *
     * @param securityToken SecurityToken em questao.
     * @return Estado atual do SecurityToken apos gravacao.
     */
    public SecurityToken addSecurityToken(SecurityToken securityToken) {
        return securityTokenRepository.save(securityToken);
    }

    /**
     * Cria um SecurityToken, com o tempo default de duracao, e o adiciona ao repositorio.
     *
     * @param user Usuario dono do token.
     * @return SecurityToken em seu estado apos gravacao.
     * @see SecurityTokenService#DEFAULT_SESSION_DURATION
     */
    public SecurityToken createAndAdd(GeanUser user) {
        SecurityToken securityToken = createSecurityToken(user);
        return addSecurityToken(securityToken);
    }

    /**
     * Cria um SecurityToken e o adiciona ao repositorio.
     *
     * @param user             Usuario dono do token.
     * @param durationInMillis Tempo de duracao deste token, in millis.
     * @return SecurityToken em seu estado apos gravacao.
     */
    public SecurityToken createAndAdd(GeanUser user, Long durationInMillis) {
        SecurityToken securityToken = createSecurityToken(user, durationInMillis);
        return addSecurityToken(securityToken);
    }

    /**
     * Verifica se o token dado pertence o usuario identificado pelo email.
     *
     * @param token     Token a ser verificado.
     * @param userEmail Email do usuario dono do token.
     * @return True caso seja autentico, false caso contrario.
     */
    public boolean checkToken(String token, String userEmail) {
        Boolean isOk = false;

        // verifica se o token existe.
        SecurityToken securityToken = securityTokenRepository.findByTokenAndEmail(token, userEmail);
        isOk = securityToken != null;

        // verifica se o token nao esta expirado.
        if (isOk) {
            isOk = !isExpired(securityToken.getToken());
        }

        return isOk;
    }


    /**
     * Carrega o SecurityToken correspondente ao token dado,
     *
     * @param token token (string).
     * @return SecurityToken correspondente,
     */
    public SecurityToken loadSecurityToken(String token) {
        return securityTokenRepository.findByToken(token);
    }

    /**
     * Cria um token de seguranca para um determinado {@see SecurityToken}
     * <p>O token segue formato "email:expires:signature".</p>
     *
     * @param securityToken instância do security token que deve ter um token gerado
     * @return Token assinado.
     */
    private String createToken(final SecurityToken securityToken) {
        StringBuilder tokenBuilder = new StringBuilder();
        tokenBuilder.append(securityToken.getUser().getEmail());
        tokenBuilder.append(":");
        tokenBuilder.append(securityToken.getExpiresIn());
        tokenBuilder.append(":");
        tokenBuilder.append(computeSignature(tokenBuilder.toString()));

        return tokenBuilder.toString();
    }

    /**
     * Assina o token dado com MD5.
     *
     * @param token Token a ser assinado.
     * @return Token assinado.
     */
    private String computeSignature(String token) {
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(token);
        signatureBuilder.append(MAGIC_KEY);

        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No MD5 algorithm available!");
        }

        return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
    }


    /**
     * Verifica se o token dado esta expirado.
     * <p>
     * A logica para verificar se o token esta expirado e':
     * <ol>
     * <li>Quebra o token por ":" (dois pontos)</li>
     * <li>Pega o pedaco da posicao 1 (timestamp de expiracao do token).</li>
     * <li>Verifica se [agora] e' maior que o timestamp de expiracao. Se for, o tempo de expiracao ja
     * passou e o token esta invalido. Caso contrario,, o token e' valido.</li>
     * </ol>
     * </p>
     * <p/>
     * <p>
     * Importante: Este metodo assume que o token esta em um formato valido (email:expiration:signature).
     * Isso devido ao fato de que ele e' chaamdo apennas pelo metodo {@link #checkToken(String, String)} passando o token recuperado do banco.
     * </p>
     *
     * @param token Token a ser verificado quanto a expiracao.
     * @return True caso token esteja expirado, False caso contrario.
     */
    public Boolean isExpired(final String token) {
        String[] pedacos = token.split(":");
        Long expires = Long.parseLong(pedacos[1]);
        long now = System.currentTimeMillis();
        return now > expires;
    }
}


