package br.com.synchro.gean.gateway.persistence.mapping;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Representa um voto dado por um usuario a uma requisicao de funcionalidade
 * .
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Entity(name = "cfm_voto")
public class Voto implements Serializable {

    /**
     * ID.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Usuario que votou.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false)
    private GeanUser voter;

    /**
     * Tipo da tipoInformacao, por exemplo, Faq, Classificacao, Pedido de Funcionalidade.
     */
    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoInformacao tipoInformacao;


    /**
     * Referencia fraca para informacao curtida (ou descurtida)
     */
    @Min(1)
    @NotNull
    @Column(nullable = false)
    private Long idInformacao;

    /**
     * True indica que o usuario curtiu; False indica o contrario.
     */
    @NotNull
    @Column(nullable = false)
    private Boolean curtiu;

    /**
     * Construtor padrao.
     */
    public Voto() {
    }


    /**
     * Construtor configura as propriedades.
     *
     * @param voter          Usuario que manifestou sua opiniao.
     * @param tipoInformacao Tipo da Informcacao
     * @param idInformacao   Referencia fraca para objeto referido.
     * @param curtiu         Curtiu ou nao.
     */
    public Voto(GeanUser voter, TipoInformacao tipoInformacao, Long idInformacao, Boolean curtiu) {
        this.voter = voter;
        this.tipoInformacao = tipoInformacao;
        this.idInformacao = idInformacao;
        this.curtiu = curtiu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GeanUser getVoter() {
        return voter;
    }

    public void setVoter(GeanUser voter) {
        this.voter = voter;
    }

    public TipoInformacao getTipoInformacao() {
        return tipoInformacao;
    }

    public void setTipoInformacao(TipoInformacao tipoInformacao) {
        this.tipoInformacao = tipoInformacao;
    }

    public Long getIdInformacao() {
        return idInformacao;
    }

    public void setIdInformacao(Long idInformacao) {
        this.idInformacao = idInformacao;
    }

    public Boolean getCurtiu() {
        return curtiu;
    }

    public void setCurtiu(Boolean curtiu) {
        this.curtiu = curtiu;
    }

    public static enum TipoInformacao {
        FAQ, CLASSIFICACAO, FUNCIONALIDADE
    }
}
