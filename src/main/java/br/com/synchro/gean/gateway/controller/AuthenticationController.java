package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.security.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Reune os controles necessarios para autenticacao dos usuarios.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Controller
@RequestMapping(value = "/authentication")
public class AuthenticationController {

    /**
     * Referencia para servico de autenticacao.
     */
    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Realiza a verificacao de autenticidade das credenciais forncecidas retornando um
     * token de acesso em caso positivo. Em caso contrario, um status 401 - Unauthorized e' retornado.
     *
     * @return 201 + token de acesso caso as credenciais sejam autenticas; 401 caso contrario.
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> authenticate(@RequestBody GeanUser user) {
        ResponseEntity<Map<String, Object>> responseEntity = new ResponseEntity<Map<String, Object>>(HttpStatus.FORBIDDEN);

        try {
            Map<String, Object> token = authenticationService.authenticate(user);
            responseEntity = new ResponseEntity<Map<String, Object>>(token, HttpStatus.CREATED);


        } catch (AuthenticationException ae) {
            responseEntity = new ResponseEntity<Map<String, Object>>(HttpStatus.FORBIDDEN);
        }

        return responseEntity;
    }
}
