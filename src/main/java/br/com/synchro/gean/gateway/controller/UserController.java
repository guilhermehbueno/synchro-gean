package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.business.service.UserService;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.security.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * Referencia para o servico de gerenciamento de usuarios.
     */
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Possibilita a inclusao de um usuario recebendo um request POST em "/user" contendo um JSON
     * de payload para instanciacao de um GeanUser. Esta instancia sera repassada ao servico de gerenciamento de usuarios
     * com a finalidade de registra-lo junto ao mecanismo de persistencia.
     *
     * @param user Usuario a ser registrado.
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> addUser(@RequestBody @Valid GeanUser user) {
        userService.addUser(user);
        Map<String, Object> token = authenticationService.authenticate(user);
        ResponseEntity<Map<String, Object>> response = new ResponseEntity<Map<String, Object>>(token, HttpStatus.CREATED);
        return response;
    }


}
