package br.com.synchro.gean.gateway.controller;

import br.com.synchro.gean.business.exception.NonUniqueUserException;
import br.com.synchro.gean.business.exception.ReferenceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.*;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@ControllerAdvice
public class ExceptionHandlerControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, Object> handleInternalServerError(Exception e) {
        return getErrors(e);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleMessageNotReadable(HttpMessageNotReadableException e) {
        return null;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return getErrors(e.getBindingResult());
    }

    @ExceptionHandler(NonUniqueUserException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleNonUniqueEmail(NonUniqueUserException nuue) {
        BindException be = new BindException(nuue.getUser(), "user");
        be.rejectValue("email", "NonUnique.user.email");
        return getErrors((BindingResult) be);
    }

    @ExceptionHandler(ReferenceNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleReferenceNotFound(ReferenceNotFoundException rnfe) {
        BindException be = new BindException(rnfe.getVoto(), "voto");
        be.rejectValue("idInformacao", "NotFound.voto.idInformacao");
        return getErrors((BindingResult) be);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleConstraintViolationException(ConstraintViolationException cve) {
        BindException root = new BindException(new Object(), "object");

        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
        for (ConstraintViolation<?> vltn : violations) {
            BindException be = new BindException(vltn.getRootBean(), "object");
            be.rejectValue(vltn.getPropertyPath().toString(), vltn.getMessageTemplate(), vltn.getMessage());
            root.addAllErrors(be);
        }

        return getErrors((BindingResult) root);
    }

    /**
     * Constroi um mapa contendo os codigos de mensagem dos erros presentes no BindingResult.
     *
     * @param br BindingResult contendo os erros.
     * @return Mapa contendo os codigos de mensagem dos erros.
     */
    private Map<String, Object> getErrors(BindingResult br) {
        Map<String, Object> m = new HashMap<String, Object>();

        List<Map<String, Object>> errors = new ArrayList<Map<String, Object>>();
        for (FieldError fe : br.getFieldErrors()) {
            Map<String, Object> errorMap = new HashMap<String, Object>();
            errorMap.put("defaultMessage", fe.getDefaultMessage());
            errorMap.put("codes", fe.getCodes());
            errorMap.put("code", fe.getCode());
            errorMap.put("field", fe.getField());
            errors.add(errorMap);
        }

        m.put("errors", errors);

        return m;
    }

    private Map<String, Object> getErrors(Exception e) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("error", e.getLocalizedMessage());
        return map;
    }


}
