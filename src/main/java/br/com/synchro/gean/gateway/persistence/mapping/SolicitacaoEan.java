package br.com.synchro.gean.gateway.persistence.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * <pre>
 * User: rfh
 * Date: 10/21/14
 *
 * Mapeamento de uma solicitação de classificação baseada em ean
 *
 * </pre>
 */
@Entity(name = "cfm_solicitacao_ean")
public class SolicitacaoEan implements Serializable {

    /**
     * Serial Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * E-mail do usuário solicitante
     */
    @NotNull
    @Size(min = 8)
    @Column(nullable = false)
    private String email;

    /**
     * Ean ainda sem ncm associada
     */
    @NotNull
    @Column(nullable = false)
    private String ean;

    /**
     * Mercadoria pendente de classificação
     */
    @NotNull
    @Size(min = 3, max = 2000)
    @Column(nullable = false)
    private String mercadoria;

    public SolicitacaoEan() {}

    public SolicitacaoEan(final String email, final String ean, final String mercadoria) {
        this.email = email;
        this.ean = ean;
        this.mercadoria = mercadoria;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(final String ean) {
        this.ean = ean;
    }

    public String getMercadoria() {
        return mercadoria;
    }

    public void setMercadoria(final String mercadoria) {
        this.mercadoria = mercadoria;
    }
}
