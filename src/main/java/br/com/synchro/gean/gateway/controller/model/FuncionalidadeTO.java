package br.com.synchro.gean.gateway.controller.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Transfer Object que serve de interface de entrada para o Controller respectivo.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.controller.FuncionalidadeController
 */
public class FuncionalidadeTO implements Serializable {

    /**
     * Descricao da funcionalidade.
     */
    @NotNull
    @Size(min = 10)
    private String descricao;

    /**
     * Construtor padrao.
     */
    public FuncionalidadeTO() {
    }

    /**
     * Construtor configura descricao da funcionalidade.
     *
     * @param descricao Descricao.
     */
    public FuncionalidadeTO(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
