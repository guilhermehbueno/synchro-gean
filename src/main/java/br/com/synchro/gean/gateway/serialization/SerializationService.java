package br.com.synchro.gean.gateway.serialization;

/**
 * <pre>
 * User: rfh
 * Date: 10/8/14
 *
 * </pre>
 */
public interface SerializationService {

    public String serialize(final Object object);

    public <T> T deserialize(final String source, final Class<T> target);

}
