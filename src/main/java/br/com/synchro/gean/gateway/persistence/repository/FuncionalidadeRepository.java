package br.com.synchro.gean.gateway.persistence.repository;

import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Repositorio responsavel pelo armazenamento e recuperacao de pedidos de funcionalidades feitos pelos usuarios.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public interface FuncionalidadeRepository extends JpaRepository<Funcionalidade, Long> {


    /**
     * Busca as funcionalidades mais votadas, retornando uma lista onde cada entrada e'
     * um array de duas posicoes: 0 -Funcionalidade, 1 - Contagem de votos.
     *
     * @param page Parametros para paginacao (offset, limit)
     * @return Lista contendo as funcionalidades mais votadas e suas respectivas contagens.
     */
    @Query("select new br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade(f, count(v)) " +
            "from br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade f, br.com.synchro.gean.gateway.persistence.mapping.Voto v " +
            "where v.idInformacao = f.id  " +
            "and v.tipoInformacao = 'FUNCIONALIDADE' " +
            "and v.curtiu = true " +
            "group by f " +
            "order by count(v) desc")
    List<Funcionalidade> findMaisVotadas(Pageable page);

    /**
     * Buscas as funcionalidades mais recentes na base.
     *
     * @param page Parametros para paginacao (offset, limit)
     * @return Lista contendo as ultimas funcionalidades registradas.
     */
    @Query("select f " +
            "from br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade f, br.com.synchro.gean.gateway.persistence.mapping.Voto v " +
            "where v.idInformacao = f.id  " +
            "and v.tipoInformacao = 'FUNCIONALIDADE' " +
            "group by f " +
            "order by f.dateCreated desc")
    List<Funcionalidade> findMaisRecentes(Pageable page);

}
