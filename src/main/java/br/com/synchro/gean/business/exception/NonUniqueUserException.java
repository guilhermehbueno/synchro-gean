package br.com.synchro.gean.business.exception;

import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;

/**
 * Exception lancada quando a identificacao do usuario (email) nao e' unica no sistema Ou seja, existe um outro usuario
 * registrado com o email em questao.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public class NonUniqueUserException extends RuntimeException {

    private GeanUser user;

    public NonUniqueUserException(GeanUser user) {
        this.user = user;
    }

    public GeanUser getUser() {
        return user;
    }
}
