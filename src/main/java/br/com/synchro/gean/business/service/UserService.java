package br.com.synchro.gean.business.service;

import br.com.synchro.gean.business.exception.NonUniqueUserException;
import br.com.synchro.gean.gateway.persistence.mapping.GeanUser;
import br.com.synchro.gean.gateway.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Reune as funcionalidades necessarias para o gerenciamento de Usuarios no sistema.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 */
@Service
@Transactional(readOnly = true)
public class UserService {

    /**
     * Referencia para repositorio de usuarios.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Referencia para passwordEncoder.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;


    /**
     * Adiciona um novo usuario ao repositorio.
     *
     * @param user Instancia a ser adicionada.
     * @return Estado da instancia apoos ser adicionada.
     * @throws br.com.synchro.gean.business.exception.NonUniqueUserException Quando o email apresentado ja esta em uso.
     */
    @Transactional(readOnly = false)
    public GeanUser addUser(GeanUser user) throws NonUniqueUserException {
        GeanUser clonedUser = user.clone();

        if (getUserByEmail(user.getEmail()) != null) {
            throw new NonUniqueUserException(clonedUser);
        }

        clonedUser.setPassword(passwordEncoder.encode(clonedUser.getPassword()));

        //neste primeiro momento, atribui o papel "USUARIO_FIRST_CLASS" a todos os usuarios entrantes.
        if (clonedUser.getRoles() == null || clonedUser.getRoles().isEmpty()) {
            List<String> roles = new ArrayList<String>();
            roles.add("USUARIO_FIRST_CLASS");
            clonedUser.setRoles(roles);
        }

        return userRepository.save(clonedUser);
    }


    /**
     * Ontem um usuario por email.
     *
     * @param strEmail Email do usuario em questao.
     * @return Usuario correspondente.
     */
    public GeanUser getUserByEmail(String strEmail) {
        GeanUser user = userRepository.findByEmail(strEmail);
        return user;
    }
}
