package br.com.synchro.gean.business.service;

import br.com.synchro.gean.gateway.persistence.mapping.Faq;
import br.com.synchro.gean.gateway.persistence.repository.FaqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Reune as funcionalidades relacionadas ao gerenciamento de Faqs.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 */
@Service
public class FaqService {

    /**
     * Referencia para repositorio de faq.
     */
    @Autowired
    private FaqRepository faqRepository;

    /**
     * Registra uma nova Faq junto ao repositorio.
     *
     * @param faq Instancia a ser registrada.
     * @return Estado da Faq apos registro.
     */
    public Faq addFaq(Faq faq) {
        return faqRepository.save(faq);
    }


    /**
     * Obtem as Faqs registradas para um dado codigo Ean.
     *
     * @param sEAN Codigo Ean.
     * @return Faqs referentes ao codigo dado.
     */
    public List<Faq> findByEanAndAnswerIsNotNull(String sEAN) {
        return faqRepository.findByEanAndAnswerIsNotNull(sEAN);
    }


}
