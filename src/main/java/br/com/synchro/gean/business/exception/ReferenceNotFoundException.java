package br.com.synchro.gean.business.exception;

import br.com.synchro.gean.gateway.persistence.mapping.Voto;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public class ReferenceNotFoundException extends RuntimeException {

    private Voto voto;

    public ReferenceNotFoundException(Voto voto) {
        this.voto = voto;
    }

    public Voto getVoto() {
        return voto;
    }
}
