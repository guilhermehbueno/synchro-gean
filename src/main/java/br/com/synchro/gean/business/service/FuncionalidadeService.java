package br.com.synchro.gean.business.service;

import br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade;
import br.com.synchro.gean.gateway.persistence.repository.FuncionalidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Reune as funcionalidades necessarias para o gerenciamento de pedidos de
 * funcionalidades feitos por nossos usuarios.
 *
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 * @see br.com.synchro.gean.gateway.persistence.mapping.Funcionalidade
 */
@Service
@Transactional(readOnly = true)
public class FuncionalidadeService {

    /**
     * Referencia para o repositorio de funcionalidades.
     */
    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;

    /**
     * Realiza o registro de um pedido de funcionalidade.
     *
     * @param fr Peddido de Funcionalidade a ser registrada.
     * @return Estado atual da entidade apos gravacao.
     */
    public Funcionalidade addFuncionalidade(@Valid Funcionalidade fr) {
        return funcionalidadeRepository.saveAndFlush(fr);
    }

    /**
     * Obtem um pedido de funcionaliade dado seu identificador.
     *
     * @param id ID da funcionalidade.
     * @return Funcionalidade correspondente.
     */
    public Funcionalidade findById(Long id) {
        return funcionalidadeRepository.findOne(id);
    }

    /**
     * Retorna uma lista contendo as top 6 funcionalidades em destaque, sendo 3 mais votadas e 3 mais recentes.
     *
     * @return Obtem as TOP 6 funcionalidades.
     */
    public List<Funcionalidade> findFuncionalidadesEmDestaque() {
        List<Funcionalidade> emDestaque = new ArrayList<Funcionalidade>(6);

        emDestaque.addAll(funcionalidadeRepository.findMaisVotadas(new PageRequest(0, 3)));
        emDestaque.addAll(funcionalidadeRepository.findMaisRecentes(new PageRequest(0, 3)));

        return emDestaque;
    }

}
