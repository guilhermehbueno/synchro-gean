package br.com.synchro.gean.business.service;

import br.com.synchro.gean.business.exception.ReferenceNotFoundException;
import br.com.synchro.gean.gateway.persistence.mapping.Voto;
import br.com.synchro.gean.gateway.persistence.repository.ClassificacaoEanRepository;
import br.com.synchro.gean.gateway.persistence.repository.FaqRepository;
import br.com.synchro.gean.gateway.persistence.repository.FuncionalidadeRepository;
import br.com.synchro.gean.gateway.persistence.repository.VotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;

/**
 * Reune as funcionalidades relacionadas a "Like" de informacoes presente no sistema.
 * <p>
 *     Disclaimer: Este conceito esta sendo chamado de Voto pois surgiu apenas como voto em pedidos de funcionalidade,
 *     posteriormente, foi ampliado para registrar um "like" em informacoes suportadas pelo TipoInformacao. No entanto,
 *     seu nome nao foi refatorado.
 * </p>
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Transactional(readOnly = true)
@Service
public class VotoService {

    /** Referencia para repositorio. */
    @Autowired
    private VotoRepository votoRepository;

    /** Referencia para repositorio de funcionalidades. */
    @Autowired
    private FuncionalidadeRepository funcionalidadeRepository;

    /** Referencia para repositorio de classficacao. */
    @Autowired
    private ClassificacaoEanRepository classificacaoEanRepository;

    /** Referencia para repositorio de faq. */
    @Autowired
    private FaqRepository faqRepository;

    /**
     * Registra um voto.
     *
     * @param voto Voto.
     * @return Estado do Voto apos gravacao.
     */
    @Transactional(readOnly = false)
    public Voto votar(@Valid Voto voto) {
        verificarReferencia(voto);
        return votoRepository.save(voto);
    }

    /**
     * TODO refactor this.
     *
     * @param voto
     */
    private void verificarReferencia(Voto voto) {
        if (voto.getIdInformacao() == null || voto.getTipoInformacao() == null) {
            throw new ReferenceNotFoundException(voto);
        }

        if (Voto.TipoInformacao.FUNCIONALIDADE.equals(voto.getTipoInformacao())) {
            if (funcionalidadeRepository.findOne(voto.getIdInformacao()) == null) {
                throw new ReferenceNotFoundException(voto);
            }
        }

        if (Voto.TipoInformacao.CLASSIFICACAO.equals(voto.getTipoInformacao())) {
            if (classificacaoEanRepository.findOne(voto.getIdInformacao()) == null) {
                throw new ReferenceNotFoundException(voto);
            }
        }

        if (Voto.TipoInformacao.FAQ.equals(voto.getTipoInformacao())) {
            if (faqRepository.findOne(voto.getIdInformacao()) == null) {
                throw new ReferenceNotFoundException(voto);
            }
        }
    }
}
