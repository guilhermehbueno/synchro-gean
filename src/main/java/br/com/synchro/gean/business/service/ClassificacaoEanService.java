package br.com.synchro.gean.business.service;

import br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan;
import br.com.synchro.gean.gateway.persistence.mapping.PesquisaClassificacao;
import br.com.synchro.gean.gateway.persistence.mapping.SolicitacaoEan;
import br.com.synchro.gean.gateway.persistence.repository.ClassificacaoEanRepository;
import br.com.synchro.gean.gateway.persistence.repository.PesquisaClassificacaoRepository;
import br.com.synchro.gean.gateway.persistence.repository.SolicitacaoEanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Reune as funcionalidades necesssarias para o gerenciamento das classificacoes para os Eans.
 *
 * @author Paulo Freitas (paulo.freitas@synchro.com.br)
 */
@Service
@Transactional(readOnly = true)
public class ClassificacaoEanService {

    /**
     * Referencia  para repositorio.
     */
    @Autowired
    private ClassificacaoEanRepository classificacaoEanRepository;

    @Autowired
    private SolicitacaoEanRepository solicitacaoEanRepository;

    @Autowired
    private PesquisaClassificacaoRepository pesquisaClassificacaoRepository;


    /**
     * Registra uma classificacao junto ao repositorio..
     *
     * @param classificacaoEan ClassificacaoEan a ser adicionada.
     * @return ClassificacaoEan.
     * @see br.com.synchro.gean.gateway.persistence.mapping.ClassificacaoEan
     */
    @Transactional(readOnly = false)
    public ClassificacaoEan addClassificacaoEan(@Valid ClassificacaoEan classificacaoEan) {
        return this.classificacaoEanRepository.save(classificacaoEan);
    }

    /**
     * Registra uma nova solicitação de classificação baseada no ean
     *
     * @param solicitacaoEan Solicitação a ser adicionada
     * @return Solicitação criada.
     * @see br.com.synchro.gean.gateway.persistence.mapping.SolicitacaoEan
     */
    @Transactional(readOnly = false)
    public SolicitacaoEan addSolicitacaoEan(@Valid SolicitacaoEan solicitacaoEan) {
        return this.solicitacaoEanRepository.save(solicitacaoEan);
    }


    /**
     * Obtem a classificacao para um dado codigo Ean.
     *
     * @param ean Codigo EAN em questao.
     * @return Classificacao referente ao Ean dado.
     */
    @Transactional(readOnly = false)
    public ClassificacaoEan findByEan(String ean) {

        ClassificacaoEan classificacaoEan = classificacaoEanRepository.findByEan(ean);

        try {
            pesquisaClassificacaoRepository.save(new PesquisaClassificacao(ean, classificacaoEan != null));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return classificacaoEan;
    }


    /**
     * Carrega o arquivo CSV na base de classificacao.
     *
     * @param stream Stream do arquivo CSV.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void load(InputStream stream) throws Exception {
        Scanner sc = new Scanner(stream);
        sc.useDelimiter("\n");

        while (sc.hasNext()) {
            String line = sc.next();
            String[] tokens = line.split(",");

            if (tokens.length == 3) {
                addClassificacaoEan(new ClassificacaoEan(tokens[0], tokens[2], tokens[1]));
            }
        }
    }

    /**
     * Obtem a quantidade de classificacoes presentes no mecanismo de persistencia.
     *
     * @return Quantidade de classificacoes.
     */
    public Long count() {
        return classificacaoEanRepository.count();
    }
}
