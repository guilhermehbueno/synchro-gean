
    alter table cfm_pedido_funcionalidade
        drop constraint FK5A4C2F3D610795AA;

    alter table cfm_security_token
        drop constraint FK767FF0AFF520E592;

    alter table cfm_user_roles
        drop constraint FK5BBDE83EF520E592;

    alter table cfm_voto
        drop constraint FKFF423DA99B3B0975;

    drop table if exists cfm_classificacao_ean cascade;

    drop table if exists cfm_solicitacao_ean cascade;

    drop table if exists cfm_faq cascade;

    drop table if exists cfm_pedido_funcionalidade cascade;

    drop table if exists cfm_pesquisa_classificacao cascade;

    drop table if exists cfm_security_token cascade;

    drop table if exists cfm_user cascade;

    drop table if exists cfm_user_roles cascade;

    drop table if exists cfm_voto cascade;

    drop table if exists cfm_voto cascade;

    drop sequence hibernate_sequence;

    create sequence hibernate_sequence;

    create table cfm_classificacao_ean (
        id int8 not null DEFAULT nextval('hibernate_sequence'),
        description varchar(2000) not null,
        ean varchar(255) not null,
        ncm varchar(255) not null,
        primary key (id)
    );

    create table cfm_solicitacao_ean (
        id int8 not null DEFAULT nextval('hibernate_sequence'),
        email varchar(255) not null,
        ean varchar(255) not null,
        mercadoria varchar(2000) not null,
        primary key (id)
    );

    create table cfm_faq (
        id int8 not null,
        answer varchar(3000),
        ean varchar(13) not null,
        question varchar(3000),
        primary key (id)
    );

    create table cfm_pedido_funcionalidade (
        id int8 not null,
        dateCreated timestamp not null,
        description varchar(255) not null,
        owner_id int4 not null,
        primary key (id)
    );

    create table cfm_pesquisa_classificacao (
        id int8 not null,
        dateCreated timestamp not null,
        ean varchar(255) not null,
        encontrou boolean,
        primary key (id)
    );

    create table cfm_security_token (
        id int4 not null,
        expiresIn int8 not null,
        token varchar(255) not null,
        user_id int4 not null,
        primary key (id)
    );

    create table cfm_user (
        id int4 not null,
        company varchar(255),
        email varchar(255) unique,
        password varchar(255),
        username varchar(255),
        primary key (id)
    );

    create table cfm_user_roles (
        user_id int4 not null,
        role varchar(255) not null
    );

    create table cfm_voto (
        id int8 not null,
        curtiu boolean not null,
        idInformacao int8 not null,
        tipoInformacao varchar(255) not null,
        voter_id int4 not null,
        primary key (id)
    );

    alter table cfm_pedido_funcionalidade
        add constraint FK5A4C2F3D610795AA
        foreign key (owner_id)
        references cfm_user;

    alter table cfm_security_token
        add constraint FK767FF0AFF520E592
        foreign key (user_id)
        references cfm_user;

    alter table cfm_user_roles
        add constraint FK5BBDE83EF520E592
        foreign key (user_id)
        references cfm_user;

    alter table cfm_voto
        add constraint FKFF423DA99B3B0975
        foreign key (voter_id)
        references cfm_user;

