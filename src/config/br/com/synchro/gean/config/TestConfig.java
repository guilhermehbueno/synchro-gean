package br.com.synchro.gean.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
@Configuration
@ComponentScan(basePackages = {
        "br.com.synchro.gean.gateway.test"
})
public class TestConfig {
}
