package br.com.synchro.gean.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Paulo Freitas (paulo.freitas@synchro,com.br)
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
    //do nothing
}