package br.com.synchro.gean.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {
        "br.com.synchro.gean.gateway.controller",
        "br.com.synchro.gean.gateway.security",
        "br.com.synchro.gean.gateway.serialization",
        "br.com.synchro.gean.business.service"
})
@Import({PersistenceConfig.class, SecurityConfig.class, WebConfig.class})
public class ApplicationConfig {

    @Resource
    private Environment environment;



}